// @flow
import * as React from 'react';
import { Provider } from 'react-redux';
import { Switch } from 'react-router-dom';
import store, { browserHistory } from 'microshopper-platform/redux/store';
import ApplicationRoute from 'microshopper-platform/components/router/ApplicationRoute';
import { ALL_ROUTES } from 'microshopper-platform/routing/routes';
import Bootstrapper from 'microshopper-platform/components/common/Bootstrapper';
import { ConnectedRouter } from 'connected-react-router';

const App = (): React.Node => (
  <Provider store={store}>
    <ConnectedRouter history={browserHistory}>
      <Bootstrapper>
        <Switch>
          {ALL_ROUTES.map(route => (
            <ApplicationRoute
              Component={route.component}
              withAuthorization={route.withAuthorization}
              authorizedRoles={route.authorizedRoles}
              restrictedRoles={route.restrictedRoles}
              redirectTo={route.redirectTo}
              exact={route.exact}
              path={route.path}
              key={route.path}
            />
          ))}
        </Switch>
      </Bootstrapper>
    </ConnectedRouter>
  </Provider>
);

export default App;
