// @flow
import * as React from 'react';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import type { ConfirmationDialog as ConfirmationDialogData } from 'microshopper-platform/redux/confirmation-dialog/types.flow';

type Props = {
  confirmationDialogData: ConfirmationDialogData,
  close: Function,
};

const ConfirmationDialog = ({ confirmationDialogData, close }: Props): React.Node => (
  <Dialog open onClose={() => close()} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
    <DialogTitle id="alert-dialog-title">{confirmationDialogData.title}</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">{confirmationDialogData.message}</DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={() => close()} color="primary" autoFocus>
        Cancel
      </Button>
      <Button
        onClick={() => {
          confirmationDialogData.callback();
          close();
        }}
        color="primary"
      >
        Yes
      </Button>
    </DialogActions>
  </Dialog>
);

export default ConfirmationDialog;
