// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { setNetworkConnectivityStatusAction } from 'microshopper-platform/redux/meta-data/slice';
import {
  onNetworkConnectivityChangesListeners,
  removeOnNetworkConnectivityChangesListeners,
  onOfflineSnackbarMessage,
  onOnlineSnackbarMessage,
} from 'microshopper-platform/utils/networkConnectivityUtils';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';

type State = {};

type Props = {
  setNetworkConnectivityStatus: Function,
  showSnackbarMessage: Function,
};

class NetworkConnectivityProvider extends React.Component<Props, State> {
  componentDidMount() {
    const { setNetworkConnectivityStatus, showSnackbarMessage } = this.props;
    onNetworkConnectivityChangesListeners(
      () => {
        setNetworkConnectivityStatus(true);
        showSnackbarMessage(onOnlineSnackbarMessage);
      },
      () => {
        setNetworkConnectivityStatus(false);
        showSnackbarMessage(onOfflineSnackbarMessage);
      },
    );
  }

  componentWillUnmount() {
    const { setNetworkConnectivityStatus, showSnackbarMessage } = this.props;
    removeOnNetworkConnectivityChangesListeners(
      () => {
        setNetworkConnectivityStatus(true);
        showSnackbarMessage(onOnlineSnackbarMessage);
      },
      () => {
        setNetworkConnectivityStatus(false);
        showSnackbarMessage(onOfflineSnackbarMessage);
      },
    );
  }

  render(): React.Node {
    return <></>;
  }
}

const mapDispatchToProps = {
  setNetworkConnectivityStatus: setNetworkConnectivityStatusAction,
  showSnackbarMessage: showSnackbarMessageAction,
};

export default compose(connect(null, mapDispatchToProps))(NetworkConnectivityProvider);
