// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { addApplicationNotificationAction } from 'microshopper-platform/redux/notification/slice';
import type { AbstractAccount } from 'microshopper-platform/redux/account/types.flow';
import { compose } from 'redux';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();
const notificationEndpoint = `${path}/api/notifications`;

type State = {};

type Props = {
  account: AbstractAccount,
  addApplicationNotification: Function,
};

class NotificationProvider extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const { account } = props;
    this.eventSource = new EventSource(`${notificationEndpoint}/${account.username}`);
  }

  componentDidMount() {
    const { addApplicationNotification } = this.props;
    this.eventSource.onmessage = (event: Object) => {
      addApplicationNotification(JSON.parse(event.data));
    };
    window.addEventListener('beforeunload', this.closeEventSource);
  }

  componentWillUnmount() {
    this.closeEventSource();
    window.removeEventListener('beforeunload', this.closeEventSource);
  }

  closeEventSource() {
    if (this.eventSource) {
      this.eventSource.close();
      this.eventSource = null;
    }
  }

  render(): React.Node {
    return <></>;
  }
}

const mapDispatchToProps = {
  addApplicationNotification: addApplicationNotificationAction,
};

export default compose(connect(null, mapDispatchToProps))(NotificationProvider);
