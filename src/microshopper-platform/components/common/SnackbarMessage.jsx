// @flow
import * as React from 'react';
import get from 'lodash/get';
import { amber, green } from '@material-ui/core/colors';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';
import CloseIcon from '@material-ui/icons/Close';
import makeStyles from '@material-ui/core/styles/makeStyles';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import type { SnackbarMessage as SnackbarMessageData } from 'microshopper-platform/redux/snackbar-message/types.flow';

export const snackbarMessageType = {
  SUCCESS: 'SUCCESS',
  WARNING: 'WARNING',
  ERROR: 'ERROR',
  INFORMATION: 'INFORMATION',
};

const autoHideDuration = 3000;

const snackbarMessageIconType = {
  SUCCESS: CheckCircleIcon,
  WARNING: WarningIcon,
  ERROR: ErrorIcon,
  INFORMATION: InfoIcon,
};

const useStyles = makeStyles(theme => ({
  SUCCESS: {
    backgroundColor: green[600],
  },
  ERROR: {
    backgroundColor: theme.palette.error.dark,
  },
  INFORMATION: {
    backgroundColor: theme.palette.primary.main,
  },
  WARNING: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

type Props = {
  snackbarMessage: SnackbarMessageData,
  close: Function,
};

const SnackbarMessage = ({ snackbarMessage, close }: Props): React.Node => {
  const classes = useStyles();
  const Icon = get(snackbarMessageIconType, snackbarMessage.type);

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open
      autoHideDuration={autoHideDuration}
      onClose={() => close()}
    >
      <SnackbarContent
        className={clsx(get(classes, snackbarMessage.type), classes.margin)}
        aria-describedby="client-snackbar"
        message={
          <span id="client-snackbar" className={classes.message}>
            <Icon className={clsx(classes.icon, classes.iconVariant)} />
            {snackbarMessage.message}
          </span>
        }
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={() => close()}>
            <CloseIcon className={classes.icon} />
          </IconButton>,
        ]}
      />
    </Snackbar>
  );
};


export default SnackbarMessage;
