// @flow
import * as React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import SnackbarMessage from 'microshopper-platform/components/common/SnackbarMessage';
import ApplicationAppBar from 'microshopper-platform/pages/common/application-appbar/ApplicationAppBar';
import ApplicationDrawer from 'microshopper-platform/pages/common/application-drawer/ApplicationDrawer';
import { connect } from 'react-redux';
import type { State } from 'microshopper-platform/redux/types.flow';
import { getSnackbarMessage, isSnackbarMessagePresent } from 'microshopper-platform/redux/snackbar-message/selectors';
import type { SnackbarMessage as SnackbarMessageData } from 'microshopper-platform/redux/snackbar-message/types.flow';
import { initializeBootstrapAction } from 'microshopper-platform/redux/bootstrap/slice';
import { getBootstrapStatus } from 'microshopper-platform/redux/bootstrap/selectors';
import type { BootstrapStatus } from 'microshopper-platform/redux/bootstrap/types.flow';
import { bootstrapStatus } from 'microshopper-platform/redux/bootstrap/types.flow';
import {
  getConfirmationDialog,
  isConfirmationDialogVisible,
} from 'microshopper-platform/redux/confirmation-dialog/selectors';
import type { ConfirmationDialog as ConfirmationDialogData } from 'microshopper-platform/redux/confirmation-dialog/types.flow';
import { closeConfirmationDialogAction } from 'microshopper-platform/redux/confirmation-dialog/slice';
import { closeSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';
import ConfirmationDialog from 'microshopper-platform/components/common/ConfirmationDialog';
import { compose } from 'redux';
import { setNetworkConnectivityStatusAction } from 'microshopper-platform/redux/meta-data/slice';
import { isConnectedToNetwork } from 'microshopper-platform/utils/networkConnectivityUtils';
import NetworkConnectivityProvider from 'microshopper-platform/components/common/NetworkConnectivityProvider';
import { listenForBeforeInstallPrompt } from 'microshopper-platform/utils/serviceWorkerUtils';

type Props = {
  children: React.Node,
  snackbarMessage: SnackbarMessageData,
  snackbarMessagePresent: boolean,
  initializeBootstrap: Function,
  status: BootstrapStatus,
  confirmationDialog: ConfirmationDialogData,
  confirmationDialogPresent: boolean,
  closeConfirmationDialog: Function,
  closeSnackbarMessage: Function,
};

const Bootstrapper = ({
  children,
  snackbarMessage,
  snackbarMessagePresent,
  initializeBootstrap,
  status,
  confirmationDialog,
  confirmationDialogPresent,
  closeConfirmationDialog,
  closeSnackbarMessage,
  setNetworkConnectivityStatus,
}: Props): React.Node => {
  React.useEffect(() => {
    setNetworkConnectivityStatus(isConnectedToNetwork);
    initializeBootstrap();
    listenForBeforeInstallPrompt();
  }, [initializeBootstrap, setNetworkConnectivityStatus]);

  const render = status === bootstrapStatus.BOOTSTRAP_SUCCEEDED;

  return (
    render && (
      <>
        <CssBaseline />
        <NetworkConnectivityProvider />
        {snackbarMessagePresent && <SnackbarMessage close={closeSnackbarMessage} snackbarMessage={snackbarMessage} />}
        {confirmationDialogPresent && (
          <ConfirmationDialog close={closeConfirmationDialog} confirmationDialogData={confirmationDialog} />
        )}
        <ApplicationAppBar />
        <ApplicationDrawer />
        {children}
      </>
    )
  );
};

const mapDispatchToProps = {
  initializeBootstrap: initializeBootstrapAction,
  closeSnackbarMessage: closeSnackbarMessageAction,
  closeConfirmationDialog: closeConfirmationDialogAction,
  setNetworkConnectivityStatus: setNetworkConnectivityStatusAction,
};

const mapStateToProps = (state: State) => ({
  snackbarMessagePresent: isSnackbarMessagePresent(state),
  snackbarMessage: getSnackbarMessage(state),
  status: getBootstrapStatus(state),
  confirmationDialogPresent: isConfirmationDialogVisible(state),
  confirmationDialog: getConfirmationDialog(state),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(Bootstrapper);
