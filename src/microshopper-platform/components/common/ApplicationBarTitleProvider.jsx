// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { setCurrentPageTitleAction } from 'microshopper-platform/redux/meta-data/slice';
import { compose } from 'redux';

type Props = {
  currentPageTitle: string,
  setCurrentPageTitle: Function,
};

const ApplicationBarTitleProvider = ({ currentPageTitle, setCurrentPageTitle }: Props): React.Node => {
  React.useEffect(() => {
    setCurrentPageTitle(currentPageTitle);
  }, [setCurrentPageTitle, currentPageTitle]);

  return <></>;
};

const mapDispatchToProps = {
  setCurrentPageTitle: setCurrentPageTitleAction,
};

export default compose(connect(null, mapDispatchToProps))(ApplicationBarTitleProvider);
