// @flow
import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

type Props = {
  children: React.Node,
  index: number,
  currentIndex: number,
};

const TabSection = ({ children, index, currentIndex }: Props): React.Node => (
  <Typography
    component="div"
    role="tabpanel"
    hidden={index !== currentIndex}
    id={`tab-section-${index}`}
    aria-labelledby={`tab-section-${index}`}
  >
    <Box p={3}>{children}</Box>
  </Typography>
);

export default TabSection;
