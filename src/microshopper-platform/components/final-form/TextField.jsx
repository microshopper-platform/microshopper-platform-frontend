// @flow
import * as React from 'react';
import TextField from '@material-ui/core/TextField';

const TextFieldWrapper = ({ input: { name, onChange, value, ...restInput }, meta, ...rest }: Object): React.Node => {
  const { helperText } = rest;
  const showError = ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched;

  const otherError = showError ? meta.error || meta.submitError : undefined;
  const showHelperText = helperText !== undefined ? helperText : otherError;

  return (
    <TextField
      {...rest}
      name={name}
      helperText={showHelperText}
      error={showError}
      inputProps={restInput}
      onChange={onChange}
      value={value}
    />
  );
};

export default TextFieldWrapper;
