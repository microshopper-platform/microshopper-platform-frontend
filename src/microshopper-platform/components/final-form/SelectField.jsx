// @flow
import * as React from 'react';
import { Field } from 'react-final-form';
import Select from 'microshopper-platform/components/final-form/Select';

const SelectField = (props: Object): React.Node => <Field {...props} component={Select} />;

export default SelectField;
