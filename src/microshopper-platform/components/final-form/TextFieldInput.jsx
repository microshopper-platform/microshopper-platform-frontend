// @flow
import * as React from 'react';
import { Field } from 'react-final-form';
import TextField from 'microshopper-platform/components/final-form/TextField';

const TextFieldInput = (props: Object): React.Node => <Field {...props} component={TextField} />;

export default TextFieldInput;
