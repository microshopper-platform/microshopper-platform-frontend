// @flow
import * as React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAccountRole, isAccountLoggedIn } from 'microshopper-platform/redux/account/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';
import type { Role } from 'microshopper-platform/redux/account/types.flow';
import { compose } from 'redux';
import type { ContextRouter } from 'react-router-dom';
import { DEFAULT_REDIRECT_TO, MAP_ROLE_WITH_REDIRECT_TO } from 'microshopper-platform/routing/routes';

type Props = {
  Component: React$ComponentType<*>,
  redirectTo?: string,
  withAuthorization: boolean,
  authorizedRoles: Array<Role>,
  restrictedRoles: Array<Role>,
  accountLoggedIn: boolean,
  accountRole: Role,
  exact: boolean,
  path: string | Array<string>,
};

const ApplicationRoute = ({
  Component,
  redirectTo,
  withAuthorization,
  accountLoggedIn,
  accountRole,
  authorizedRoles,
  restrictedRoles,
  exact,
  path,
}: Props): React.Node => {
  const authorized = React.useMemo<boolean>((): boolean => {
    if (withAuthorization) {
      return accountLoggedIn && authorizedRoles.some(role => role === accountRole);
    }
    return true;
  }, [withAuthorization, accountLoggedIn, authorizedRoles, accountRole]);
  const restricted = React.useMemo<boolean>(
    (): boolean => accountLoggedIn && restrictedRoles.some(role => role === accountRole),
    [accountLoggedIn, restrictedRoles, accountRole],
  );
  const resolvedRedirectTo = React.useMemo<string>((): string => {
    if (redirectTo) {
      return redirectTo;
    }
    if (accountLoggedIn) {
      return MAP_ROLE_WITH_REDIRECT_TO.get(accountRole);
    }
    return DEFAULT_REDIRECT_TO;
  }, [redirectTo, accountLoggedIn, accountRole]);
  return (
    <Route
      exact={exact}
      path={path}
      render={(router: ContextRouter) =>
        authorized && !restricted ? (
          <Component {...router} />
        ) : (
          <Redirect to={{ pathname: resolvedRedirectTo, state: { from: router.location } }} />
        )}
    />
  );
};

const mapStateToProps = (state: State) => ({
  accountLoggedIn: isAccountLoggedIn(state),
  accountRole: getAccountRole(state),
});

export default compose(connect(mapStateToProps, null))(ApplicationRoute);
