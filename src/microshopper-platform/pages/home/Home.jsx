// @flow
import * as React from 'react';
import type { Manufacturer, Product } from 'microshopper-platform/api/generated-sources/product-catalog/src';
import {
  getTopManufacturers,
  getTopReviewedProducts,
  getTopSoldProducts,
} from 'microshopper-platform/services/product-catalog';
import TopSoldProductsSection from 'microshopper-platform/pages/home/top-sections/TopSoldProductsSection';
import TopReviewedProductsSection from 'microshopper-platform/pages/home/top-sections/TopReviewedProductsSection';
import TopManufacturersSection from 'microshopper-platform/pages/home/top-sections/TopManufacturersSection';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Container from '@material-ui/core/Container';
import TabSection from 'microshopper-platform/components/common/TabSection';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles({
  root: {
    justifyContent: 'center',
  },
  scroller: {
    flexGrow: '0',
  },
});

const Home = (): React.Node => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [fetching, setFetching] = React.useState<boolean>(true);
  const [topSoldProducts, setTopSoldProducts] = React.useState<Array<Product>>([]);
  const [topReviewedProducts, setTopReviewedProducts] = React.useState<Array<Product>>([]);
  const [topManufacturers, setTopManufacturers] = React.useState<Array<Manufacturer>>([]);
  const [tabIndex, setTabIndex] = React.useState<number>(0);

  const handleTabChange = React.useCallback((event: Object, newValue: number) => {
    setTabIndex(newValue);
  }, []);

  React.useEffect(() => {
    dispatch(showProgressAction());
    getTopSoldProducts(5).then((topSoldProductsData: Array<Product>) => setTopSoldProducts(topSoldProductsData));
    getTopReviewedProducts(5).then((topReviewedProductsData: Array<Product>) =>
      setTopReviewedProducts(topReviewedProductsData),
    );
    getTopManufacturers(5)
      .then((topManufacturersData: Array<Manufacturer>) => setTopManufacturers(topManufacturersData))
      .finally(() => {
        dispatch(hideProgressAction());
      });
    setFetching(false);
  }, [dispatch]);

  if (fetching) {
    return null;
  }

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Home" />
      <Paper>
        <Tabs
          classes={{ root: classes.root, scroller: classes.scroller }}
          variant="scrollable"
          scrollButtons="auto"
          value={tabIndex}
          onChange={handleTabChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab wrapped label="Top 5 Sold Products" />
          <Tab wrapped label="Top 5 Reviewed Products" />
          <Tab wrapped label="Top 5 Manufacturers Products" />
        </Tabs>
        <TabSection index={0} currentIndex={tabIndex}>
          <TopSoldProductsSection products={topSoldProducts} />
        </TabSection>
        <TabSection index={1} currentIndex={tabIndex}>
          <TopReviewedProductsSection products={topReviewedProducts} />
        </TabSection>
        <TabSection index={2} currentIndex={tabIndex}>
          <TopManufacturersSection manufacturers={topManufacturers} />
        </TabSection>
      </Paper>
    </Container>
  );
};

export default Home;
