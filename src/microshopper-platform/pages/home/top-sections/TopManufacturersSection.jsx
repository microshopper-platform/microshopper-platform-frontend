// @flow
import * as React from 'react';
import type { Manufacturer } from 'microshopper-platform/api/generated-sources/product-catalog/src';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ManufacturerCard from 'microshopper-platform/pages/common/ManufacturerCard';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
    position: 'relative',
    overflow: 'auto',
    maxHeight: '80vh',
    '&::-webkit-scrollbar': {
      width: '0.4em',
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
      outline: '1px solid slategrey',
    },
  },
}));

type Props = {
  manufacturers: Array<Manufacturer>,
};

const TopManufacturersSection = ({ manufacturers }: Props): React.Node => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Grid container direction="column" justify="center" alignItems="center">
        <Typography variant="h4">Top 5 Manufacturers</Typography>
        <Grid container spacing={5} direction="column" justify="center" alignItems="center">
          {manufacturers.map(manufacturer => (
            <Grid key={manufacturer.id} item xs={12} sm={8}>
              <ManufacturerCard manufacturer={manufacturer} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Container>
  );
};

export default TopManufacturersSection;
