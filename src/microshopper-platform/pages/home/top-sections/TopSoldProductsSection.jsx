// @flow
import * as React from 'react';
import type { Product } from 'microshopper-platform/api/generated-sources/product-catalog/src';
import Grid from '@material-ui/core/Grid';
import ProductCard from 'microshopper-platform/pages/common/ProductCard';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
    position: 'relative',
    overflow: 'auto',
    maxHeight: '80vh',
    '&::-webkit-scrollbar': {
      width: '0.4em',
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
      outline: '1px solid slategrey',
    },
  },
}));

type Props = {
  products: Array<Product>,
};

const TopSoldProductsSection = ({ products }: Props): React.Node => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <Grid container direction="column" justify="center" alignItems="center">
        <Typography variant="h4">Top 5 Sold Products</Typography>
        <Grid container spacing={5} direction="column" justify="center" alignItems="center">
          {products.map(product => (
            <Grid key={product.id} item xs={12} sm={8}>
              <ProductCard product={product} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Container>
  );
};

export default TopSoldProductsSection;
