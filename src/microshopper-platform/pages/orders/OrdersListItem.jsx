// @flow
import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import type { Order } from 'microshopper-platform/api/generated-sources/order/src';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import MarkunreadMailboxIcon from '@material-ui/icons/MarkunreadMailbox';
import DetailsIcon from '@material-ui/icons/Details';
import Chip from '@material-ui/core/Chip';
import { Link } from 'react-router-dom';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textItem: {
    flexShrink: 2,
  },
}));

type Props = {
  item: Order,
};

const OrdersListItem = ({ item }: Props): React.Node => {
  const classes = useStyles();
  return (
    <ListItem className={classes.root}>
      <ListItemIcon>
        <MarkunreadMailboxIcon />
      </ListItemIcon>
      <ListItemText className={classes.textItem} primary={`Order ID: ${item.id}`} />
      <Chip label={item.status} />
      <ListItemSecondaryAction>
        <IconButton component={Link} to={`/order/${item.id}`} edge="end" aria-label="Order Details">
          <DetailsIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default OrdersListItem;
