// @flow
import * as React from 'react';
import Box from '@material-ui/core/Box';
import GoogleMapReact from 'google-map-react';
import withStyles from '@material-ui/core/styles/withStyles';
import isEmpty from 'lodash/isEmpty';
import OrderMarker from 'microshopper-platform/pages/orders/order-tracking/OrderMarker';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const styles = {
  mapContainer: {
    height: '100vh',
    width: '100%',
  },
};

const path = resolveGatewayLocation();
const orderTrackingEndpoint = `${path}/api/orders/track`;

type Location = {
  latitude: number,
  longitude: number,
};

type State = {
  location: Location,
  zoom: number,
};

type Props = {
  orderId: string,
  classes: Object,
};

class TrackOrder extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const { orderId } = props;
    this.state = {
      zoom: 11,
    };
    this.eventSource = new EventSource(`${orderTrackingEndpoint}/${orderId}`);
  }

  componentDidMount() {
    this.eventSource.onmessage = (event: Object) => {
      const data = JSON.parse(event.data);
      this.setState({
        location: {
          latitude: data.latitude,
          longitude: data.longitude,
        },
      });
    };
    window.addEventListener('beforeunload', this.closeEventSource);
  }

  componentWillUnmount() {
    this.closeEventSource();
    window.removeEventListener('beforeunload', this.closeEventSource);
  }

  closeEventSource() {
    if (this.eventSource) {
      this.eventSource.close();
      this.eventSource = null;
    }
  }

  render(): React.Node {
    const { location }: { location: Location } = this.state;

    if (isEmpty(location)) {
      return null;
    }

    const { classes } = this.props;
    const { zoom }: { center: Location, zoom: number } = this.state;

    return (
      <Box className={classes.mapContainer}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY }}
          center={{
            lat: location.latitude,
            lng: location.longitude,
          }}
          defaultZoom={zoom}
        >
          {!isEmpty(location) && <OrderMarker lat={location.latitude} lng={location.longitude} text="Order Marker" />}
        </GoogleMapReact>
      </Box>
    );
  }
}

export default withStyles(styles)(TrackOrder);
