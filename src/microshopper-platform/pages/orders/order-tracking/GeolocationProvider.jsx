// @flow
import * as React from 'react';
import type { UpdateOrderLocation } from 'microshopper-platform/api/generated-sources/order/src';
import { updateOrderLocation } from 'microshopper-platform/services/order';
import { clearWatch, watchPosition } from 'microshopper-platform/utils/geoLocationUtils';
import { releaseWakeLock, requestWakeLock } from 'microshopper-platform/utils/wakeLockUtils';
import {disableVisibilityChange, enableVisibilityChange} from "../../../utils/wakeLockUtils";

type State = {
  geolocationWatchId: number,
};

type Props = {
  orderId: string,
};

class GeolocationProvider extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      geolocationWatchId: -1,
    };
  }

  componentDidMount() {
    const { orderId } = this.props;
    const setCurrentPositionCallback = (position: Position) => {
      const newOrderLocation: UpdateOrderLocation = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      };
      updateOrderLocation(orderId, newOrderLocation);
    };
    const id = watchPosition(setCurrentPositionCallback);
    requestWakeLock('screen'); // wakelock
    enableVisibilityChange(); // wakelock
    this.setState({ geolocationWatchId: id });
    window.addEventListener('beforeunload', this.stopTracking);
  }

  componentWillUnmount() {
    this.stopTracking();
    window.removeEventListener('beforeunload', this.stopTracking);
  }

  stopTracking() {
    clearWatch(this.state.geolocationWatchId);
    releaseWakeLock(); // wakelock
    disableVisibilityChange(); // wakelock
  }

  render(): React.Node {
    return <></>;
  }
}

export default GeolocationProvider;
