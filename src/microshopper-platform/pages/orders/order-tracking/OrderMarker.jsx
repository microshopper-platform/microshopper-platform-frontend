// @flow
import * as React from 'react';
import RoomIcon from '@material-ui/icons/Room';
import Box from '@material-ui/core/Box';
import makeStyles from '@material-ui/core/styles/makeStyles';

const K_WIDTH = 40;
const K_HEIGHT = 40;

const useStyles = makeStyles(() => ({
  marker: {
    position: 'absolute',
    width: K_WIDTH,
    height: K_HEIGHT,
    left: -K_WIDTH / 2,
    top: -K_HEIGHT / 2,
  },
}));

const OrderMarker = (): React.Node => {
  const classes = useStyles();

  return (
    <Box component="div" className={classes.marker}>
      <RoomIcon color="inherit" fontSize="large" />
    </Box>
  );
};

export default OrderMarker;
