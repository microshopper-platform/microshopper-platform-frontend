// @flow
import * as React from 'react';
import type {
  AbstractAccount,
  AccountData,
  ManufacturerAccountData,
} from 'microshopper-platform/redux/account/types.flow';
import List from '@material-ui/core/List';
import Container from '@material-ui/core/Container';
import {
  getAllOrdersForManufacturerId,
  getAllOrdersForUsername,
  getAllOrdersForTransporterAssingee,
} from 'microshopper-platform/services/order';
import type { Order } from 'microshopper-platform/api/generated-sources/order/src';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import Typography from '@material-ui/core/Typography';
import OrdersListItem from 'microshopper-platform/pages/orders/OrdersListItem';
import type { State } from 'microshopper-platform/redux/types.flow';
import { getAccount, getAccountData } from 'microshopper-platform/redux/account/selectors';
import { connect, useDispatch } from 'react-redux';
import { compose } from 'redux';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import { isManufacturerAdmin, isManufacturerTransporter, isUserRole } from 'microshopper-platform/utils/roleUtils';

type Props = {
  account: AbstractAccount,
  accountData: AccountData & ManufacturerAccountData,
};

const OrdersPage = ({ account, accountData }: Props): React.Node => {
  const dispatch = useDispatch();
  const [orders, setOrders] = React.useState<Array<Order>>([]);

  React.useEffect(() => {
    dispatch(showProgressAction());
    if (isUserRole(account.role)) {
      getAllOrdersForUsername(account.username)
        .then((ordersData: Array<Order>) => {
          setOrders(ordersData);
        })
        .finally(() => {
          dispatch(hideProgressAction());
        });
    } else if (isManufacturerAdmin(account.role)) {
      getAllOrdersForManufacturerId(accountData.id)
        .then((ordersData: Array<Order>) => {
          setOrders(ordersData);
        })
        .finally(() => {
          dispatch(hideProgressAction());
        });
    } else if (isManufacturerTransporter(account.role)) {
      getAllOrdersForTransporterAssingee(account.username)
        .then((ordersData: Array<Order>) => {
          setOrders(ordersData);
        })
        .finally(() => {
          dispatch(hideProgressAction());
        });
    }
  }, [account, accountData, dispatch]);

  const noOrdersNode = (
    <Typography align="center" variant="h5">
      Your have no orders yet.
    </Typography>
  );

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Orders" />
      {orders.length === 0 ? (
        noOrdersNode
      ) : (
        <List>
          {orders.map(order => (
            <OrdersListItem key={order.id} item={order} />
          ))}
        </List>
      )}
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  account: getAccount(state),
  accountData: getAccountData(state),
});

export default compose(connect(mapStateToProps, null))(OrdersPage);
