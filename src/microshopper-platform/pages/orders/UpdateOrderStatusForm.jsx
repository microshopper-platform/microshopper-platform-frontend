// @flow
import * as React from 'react';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import UpdateIcon from '@material-ui/icons/Update';
import { Form } from 'react-final-form';
import { updateOrderStatus } from 'microshopper-platform/services/order';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import type { Order, OrderStatusEnum } from 'microshopper-platform/api/generated-sources/order/src';
import SelectField from 'microshopper-platform/components/final-form/SelectField';

type FormProps = {
  status: OrderStatusEnum,
};

type Props = {
  order: Order,
  setOrderCallback: Function,
};

const UpdateOrderStatusForm = ({ order, setOrderCallback }: Props): React.Node => {
  const { showSnackbarMessage } = useSnackbarMessage();
  const handleUpdateOrderStatus = React.useCallback(
    (formValues: FormProps) => {
      updateOrderStatus(order.id, formValues.status)
        .then(() => {
          showSnackbarMessage('Successfully updated the order status', snackbarMessageType.SUCCESS, false);
          setOrderCallback({ ...order, status: formValues.status });
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while updating the order status.', snackbarMessageType.ERROR, false);
        });
    },
    [order, setOrderCallback, showSnackbarMessage],
  );

  const initialValues: FormProps = {
    status: order.status,
  };

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleUpdateOrderStatus}
      render={({ handleSubmit }) => (
        <FormControl fullWidth margin="dense">
          <SelectField name="status" label="Select status" autoWidth>
            <MenuItem value="REQUESTED">Requested</MenuItem>
            <MenuItem value="IN_PROGRESS">In Progress</MenuItem>
            <MenuItem value="TRACEABLE">Traceable</MenuItem>
            <MenuItem value="COMPLETED">Completed</MenuItem>
            <MenuItem value="CANCELED">Canceled</MenuItem>
          </SelectField>
          <Button variant="contained" color="primary" size="large" startIcon={<UpdateIcon />} onClick={handleSubmit}>
            Update Status
          </Button>
        </FormControl>
      )}
    />
  );
};

export default UpdateOrderStatusForm;
