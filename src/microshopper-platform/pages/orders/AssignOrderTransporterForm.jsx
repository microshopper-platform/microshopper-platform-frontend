// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { useDispatch } from 'react-redux';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import { Form } from 'react-final-form';
import type { ManufacturerAccount } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { getAllManufacturerTransporters } from 'microshopper-platform/services/manufacturer';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import { assignOrderTransporter } from 'microshopper-platform/services/order';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import SelectField from 'microshopper-platform/components/final-form/SelectField';

const useStyles = makeStyles(() => ({
  assignTransporterForm: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignContent: 'space-between',
    flexWrap: 'wrap',
  },
  transporterSelect: {
    width: '50%',
    margin: 10,
  },
  assignButton: {
    width: '50%',
    margin: 10,
  },
}));

type FormProps = {
  transporterAssignee: string,
};

type Props = {
  orderId: string,
  manufacturerId: number,
};

const AssignOrderTransporterForm = ({ orderId, manufacturerId }: Props): React.Node => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { showSnackbarMessage } = useSnackbarMessage();
  const [transporters, setTransporters] = React.useState<Array<ManufacturerAccount>>([]);

  React.useEffect(() => {
    dispatch(showProgressAction());
    getAllManufacturerTransporters(manufacturerId)
      .then((transportersData: Array<ManufacturerAccount>) => {
        setTransporters(transportersData);
      })
      .finally(() => {
        dispatch(hideProgressAction());
      });
  }, [dispatch, manufacturerId]);

  const handleAssignOrderTransporter = React.useCallback(
    (formValues: FormProps) => {
      assignOrderTransporter(orderId, formValues.transporterAssignee)
        .then(() => {
          showSnackbarMessage('Assign order transporter successful.', snackbarMessageType.SUCCESS, false);
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while assigning order transporter.', snackbarMessageType.ERROR, false);
        });
    },
    [showSnackbarMessage, orderId],
  );

  return (
    <Form
      onSubmit={handleAssignOrderTransporter}
      render={({ handleSubmit }) => (
        <Box component="div" className={classes.assignTransporterForm}>
          <FormControl className={classes.transporterSelect}>
            <SelectField label="Transporter" id="transporter" name="transporterAssignee">
              {transporters.map(transporter => (
                <MenuItem key={transporter.username} value={transporter.username}>
                  {transporter.username}
                </MenuItem>
              ))}
            </SelectField>
          </FormControl>
          <Button
            className={classes.assignButton}
            fullWidth
            size="small"
            variant="contained"
            onClick={handleSubmit}
            color="primary"
            startIcon={<AssignmentIndIcon />}
          >
            Assign Transporter
          </Button>
        </Box>
      )}
    />
  );
};

export default AssignOrderTransporterForm;
