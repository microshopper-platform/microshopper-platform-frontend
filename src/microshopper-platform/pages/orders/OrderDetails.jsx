// @flow
import * as React from 'react';
import isEmpty from 'lodash/isEmpty';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import type { Order, Product } from 'microshopper-platform/api/generated-sources/order/src';
import type { Product as ProductData } from 'microshopper-platform/api/generated-sources/product/src';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import { connect, useDispatch } from 'react-redux';
import type { State } from 'microshopper-platform/redux/types.flow';
import { orderStatus } from 'microshopper-platform/utils/constants';
import { cancelOrder, getOrderById } from 'microshopper-platform/services/order';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import CancelScheduleSendIcon from '@material-ui/icons/CancelScheduleSend';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TrackOrder from 'microshopper-platform/pages/orders/order-tracking/TrackOrder';
import ExploreIcon from '@material-ui/icons/Explore';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import type { Match } from 'react-router-dom';
import GeolocationProvider from 'microshopper-platform/pages/orders/order-tracking/GeolocationProvider';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import DescriptionIcon from '@material-ui/icons/Description';
import { getProduct } from 'microshopper-platform/services/product';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import {
  isManufacturerAdmin,
  isManufacturerRole,
  isManufacturerTransporter,
  isUserRole,
} from 'microshopper-platform/utils/roleUtils';
import AssignOrderTransporterForm from 'microshopper-platform/pages/orders/AssignOrderTransporterForm';
import { getAccount } from 'microshopper-platform/redux/account/selectors';
import type { AbstractAccount } from 'microshopper-platform/redux/account/types.flow';
import UpdateOrderStatusForm from './UpdateOrderStatusForm';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

type Props = {
  account: AbstractAccount,
  match: Match,
};

const OrderDetails = ({ account, match }: Props): React.Node => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { showSnackbarMessage } = useSnackbarMessage();
  const { orderId } = match.params;
  const [products, setProducts] = React.useState<Array<ProductData>>([]);
  const [trackOrder, setTrackOrder] = React.useState<boolean>(false);
  const [enableOrderLocation, setEnableOrderLocation] = React.useState<boolean>(false);
  const [order, setOrder] = React.useState<Order>({});
  const [isFetching, setIsFetching] = React.useState<boolean>(true);

  React.useEffect((): Function => {
    dispatch(showProgressAction());
    getOrderById(orderId)
      .then((orderData: Order) => {
        setOrder(orderData);
      })
      .finally(() => {
        dispatch(hideProgressAction());
      });
  }, [orderId, dispatch]);

  React.useEffect(() => {
    if (!isEmpty(order.products)) {
      const pending: Array<ProductData> = order.products.map(product => getProduct(product.productId));
      Promise.all(pending)
        .then((productsData: ProductData) => {
          setProducts(productsData);
          setIsFetching(false);
        })
        .finally(() => {
          dispatch(hideProgressAction());
        });
    }
  }, [order.products, dispatch]);

  const handleCancelOrder = React.useCallback(() => {
    cancelOrder(order.id)
      .then(() => {
        showSnackbarMessage('Successfully canceled the order', snackbarMessageType.SUCCESS, false);
      })
      .catch(() => {
        showSnackbarMessage('Error occurred while canceling the order.', snackbarMessageType.ERROR, false);
      });
  }, [order, showSnackbarMessage]);

  const setOrderCallback = React.useCallback(
    (newOrder: Order) => {
      setOrder(newOrder);
    },
    [setOrder],
  );

  const canEnableLocationForOrder =
    isManufacturerTransporter(account.role) &&
    order.transporterAssignee === account.username &&
    order.status === orderStatus.TRACEABLE;

  if (isFetching) {
    return null;
  }

  return (
    <Container>
      <Card className={classes.root}>
        <ApplicationBarTitleProvider currentPageTitle="Order Details" />
        <Grid container direction="column" justify="center" alignItems="stretch">
          <Grid container direction="row" justify="space-evenly" alignItems="stretch">
            <Box component="div">
              <Typography variant="h5">Order Details</Typography>
              <Typography>{`Order ID: ${order.id}`}</Typography>
              <Typography>Order Products:</Typography>
              <List>
                {order.products.map((product: Product) => (
                  <ListItem key={product.productId}>
                    <ListItemIcon>
                      <DescriptionIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary={`Name: ${products.find(p => p.id === product.productId).name}`}
                      secondary={`Quantity: ${product.quantity}`}
                    />
                  </ListItem>
                ))}
              </List>
            </Box>
            <Box component="div">
              {isUserRole(account.role) && (
                <ButtonGroup variant="contained" color="primary" className={classes.margin}>
                  <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    disabled={order.status !== orderStatus.REQUESTED}
                    startIcon={<CancelScheduleSendIcon />}
                    onClick={() => handleCancelOrder()}
                  >
                    Cancel Order
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    disabled={order.status !== orderStatus.TRACEABLE}
                    startIcon={<ExploreIcon />}
                    onClick={() => setTrackOrder(!trackOrder)}
                  >
                    Track Order
                  </Button>
                </ButtonGroup>
              )}
              {isManufacturerRole(account.role) && (
                <>
                  <ButtonGroup variant="contained" color="primary" className={classes.margin}>
                    <Button
                      variant="contained"
                      color="primary"
                      size="large"
                      disabled={!canEnableLocationForOrder || enableOrderLocation}
                      startIcon={<ExploreIcon />}
                      onClick={() => setEnableOrderLocation(true)}
                    >
                      Enable Order Location
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      size="large"
                      disabled={!enableOrderLocation}
                      startIcon={<ExploreIcon />}
                      onClick={() => setEnableOrderLocation(false)}
                    >
                      Disable Order Location
                    </Button>
                  </ButtonGroup>
                  <UpdateOrderStatusForm order={order} setOrderCallback={setOrderCallback} />
                </>
              )}
              {isManufacturerAdmin(account.role) && (
                <AssignOrderTransporterForm orderId={orderId} manufacturerId={order.manufacturerId} />
              )}
            </Box>
          </Grid>
          {trackOrder && (
            <Box component="div">
              <TrackOrder orderId={order.id} />
            </Box>
          )}
        </Grid>
      </Card>
      {enableOrderLocation && <GeolocationProvider orderId={order.id} />}
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  account: getAccount(state),
});

export default compose(connect(mapStateToProps, null), withRouter)(OrderDetails);
