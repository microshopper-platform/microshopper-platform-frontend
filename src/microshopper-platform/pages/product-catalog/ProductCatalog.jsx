// @flow
import * as React from 'react';
import isEmpty from 'lodash/isEmpty';
import has from 'lodash/has';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { getAllCategories, searchProducts } from 'microshopper-platform/services/product';
import ProductCard from 'microshopper-platform/pages/common/ProductCard';
import type { Product, Category } from 'microshopper-platform/api/generated-sources/product/src';
import type { SearchProps } from 'microshopper-platform/pages/product-catalog/types.flow';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import type { Manufacturer } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { getAllManufacturers } from 'microshopper-platform/services/manufacturer';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Form } from 'react-final-form';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import SelectField from 'microshopper-platform/components/final-form/SelectField';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import SearchIcon from '@material-ui/icons/Search';
import type { Location } from 'react-router-dom';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(() => ({
  searchForm: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignContent: 'space-between',
    flexWrap: 'wrap',
  },
  nameTextField: {
    width: '50%',
    margin: 10,
  },
  categorySelect: {
    width: '50%',
    margin: 10,
  },
  manufacturerSelect: {
    width: '50%',
    margin: 10,
  },
  searchButton: {
    width: '50%',
    margin: 10,
  },
}));

type Props = {
  location: Location,
};

const ProductCatalog = ({ location }: Props): React.Node => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [categories, setCategories] = React.useState<Array<Category>>([]);
  const [manufacturers, setManufacturers] = React.useState<Array<Manufacturer>>([]);
  const [products, setProducts] = React.useState<Array<Product>>([]);

  // TODO: Fix a console warning regarding this with manufacturers select
  const initialValues = React.useMemo<SearchProps>((): SearchProps => {
    if (!isEmpty(location.state) && has(location.state, 'searchProps')) {
      const { searchProps }: { searchProps: SearchProps } = location.state;
      return {
        name: searchProps.name ? searchProps.name : undefined,
        categoryId: searchProps.categoryId ? searchProps.categoryId : undefined,
        manufacturerId: searchProps.manufacturerId ? searchProps.manufacturerId : undefined,
      };
    }
    return {
      name: undefined,
      categoryId: undefined,
      manufacturerId: undefined,
    };
  }, [location.state]);

  React.useEffect(() => {
    dispatch(showProgressAction());
    searchProducts(initialValues).then((productsData: Array<Product>) => {
      setProducts(productsData);
    });
    getAllManufacturers().then((manufacturersData: Array<Manufacturer>) => setManufacturers(manufacturersData));
    getAllCategories()
      .then((categoriesData: Array<Category>) => setCategories(categoriesData))
      .finally(() => {
        dispatch(hideProgressAction());
      });
  }, [dispatch, initialValues]);

  const handleSearchClick = React.useCallback(
    (formValues: SearchProps) => {
      dispatch(showProgressAction());
      searchProducts(formValues)
        .then((productsData: Array<Product>) => {
          setProducts(productsData);
        })
        .finally(() => {
          dispatch(hideProgressAction());
        });
    },
    [dispatch],
  );

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Product Catalog" />
      <Form
        initialValues={initialValues}
        onSubmit={handleSearchClick}
        render={({ handleSubmit }) => (
          <Box component="div" className={classes.searchForm}>
            <TextFieldInput className={classes.nameTextField} id="name" label="Name" name="name" type="text" />
            <FormControl className={classes.categorySelect}>
              <SelectField label="Category" id="categoryId" name="categoryId">
                {categories.map(category => (
                  <MenuItem key={category.id} value={category.id}>
                    {category.name}
                  </MenuItem>
                ))}
              </SelectField>
            </FormControl>
            <FormControl className={classes.manufacturerSelect}>
              <SelectField labelId="manufacturerLabel" id="manufacturerId" name="manufacturerId" label="Manufacturer">
                {manufacturers.map(manufacturer => (
                  <MenuItem key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </MenuItem>
                ))}
              </SelectField>
            </FormControl>
            <Button
              className={classes.searchButton}
              fullWidth
              size="small"
              variant="contained"
              onClick={handleSubmit}
              color="primary"
              startIcon={<SearchIcon />}
            >
              Search Products
            </Button>
          </Box>
        )}
      />
      <Grid container spacing={2} direction="row" justify="flex-start" alignItems="flex-start">
        {products.map(product => (
          <Grid key={product.id} item xs={12} sm={4}>
            <ProductCard product={product} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default compose(withRouter)(ProductCatalog);
