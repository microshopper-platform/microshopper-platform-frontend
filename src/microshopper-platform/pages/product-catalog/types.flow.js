// @flow

export type SearchProps = {
  name?: string,
  categoryId?: number | string,
  manufacturerId?: number | string,
};
