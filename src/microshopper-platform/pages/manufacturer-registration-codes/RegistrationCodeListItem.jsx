// @flow
import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Chip from '@material-ui/core/Chip';
import makeStyles from '@material-ui/core/styles/makeStyles';
import type { RegistrationCode } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import VpnKeyIcon from '@material-ui/icons/VpnKey';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textItem: {
    flexShrink: 2,
  },
}));

type Props = {
  item: RegistrationCode,
};

const RegistrationCodeListItem = ({ item }: Props): React.Node => {
  const classes = useStyles();
  return (
    <ListItem className={classes.root}>
      <ListItemIcon>
        <VpnKeyIcon />
      </ListItemIcon>
      <ListItemText className={classes.textItem} primary={`Code: ${item.code}`} />
      <Chip label={item.role} />
      {item.used ? <Chip label="USED" /> : <Chip label="NEW" />}
    </ListItem>
  );
};

export default RegistrationCodeListItem;
