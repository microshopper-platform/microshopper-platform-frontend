// @flow
import * as React from 'react';
import type { ManufacturerAccountData } from 'microshopper-platform/redux/account/types.flow';
import { connect, useDispatch } from 'react-redux';
import type {
  RegistrationCode,
  GenerateRegistrationCode,
} from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { getAllRegistrationCodes, generateNewRegistrationCode } from 'microshopper-platform/services/manufacturer';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import List from '@material-ui/core/List';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { Form } from 'react-final-form';
import makeStyles from '@material-ui/core/styles/makeStyles';
import RegistrationCodeListItem from 'microshopper-platform/pages/manufacturer-registration-codes/RegistrationCodeListItem';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import { accountRoles, manufacturerRoles } from 'microshopper-platform/utils/constants';
import SelectField from 'microshopper-platform/components/final-form/SelectField';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import { compose } from 'redux';
import { getAccountData } from 'microshopper-platform/redux/account/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';

const useStyles = makeStyles(() => ({
  newCodeForm: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignContent: 'space-between',
    flexWrap: 'wrap',
  },
  roleSelect: {
    width: '50%',
    margin: 10,
  },
  generateButton: {
    width: '50%',
    margin: 10,
  },
}));

type FormProps = {
  role: $Keys<typeof accountRoles>,
};

type Props = {
  manufacturerAccountData: ManufacturerAccountData,
};

const ManufacturerRegistrationCodes = ({ manufacturerAccountData }: Props): React.Node => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { showSnackbarMessage } = useSnackbarMessage();
  const [codes, setCodes] = React.useState<Array<RegistrationCode>>([]);

  React.useEffect(() => {
    dispatch(showProgressAction());
    getAllRegistrationCodes(manufacturerAccountData.id)
      .then((codesData: Array<RegistrationCode>) => {
        setCodes(codesData);
      })
      .finally(() => {
        dispatch(hideProgressAction());
      });
  }, [dispatch, manufacturerAccountData.id]);

  const noOrdersNode = (
    <Typography align="center" variant="h5">
      Your have no registration codes yet.
    </Typography>
  );

  const handleGenerateNewRegistrationCode = React.useCallback(
    (formValues: FormProps) => {
      const generateRegistrationCode: GenerateRegistrationCode = {
        manufacturerId: manufacturerAccountData.id,
        role: formValues.role,
      };
      generateNewRegistrationCode(generateRegistrationCode)
        .then((newCode: RegistrationCode) => {
          showSnackbarMessage('Generate registration code successful', snackbarMessageType.SUCCESS, false);
          setCodes([...codes, newCode]);
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while generating registration code.', snackbarMessageType.ERROR, false);
        });
    },
    [codes, manufacturerAccountData.id, showSnackbarMessage],
  );

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Registration Codes" />
      <Form
        onSubmit={handleGenerateNewRegistrationCode}
        render={({ handleSubmit }) => (
          <Box component="div" className={classes.newCodeForm}>
            <FormControl className={classes.roleSelect}>
              <SelectField label="Role" id="role" name="role">
                {manufacturerRoles.map(role => (
                  <MenuItem key={role} value={role}>
                    {role}
                  </MenuItem>
                ))}
              </SelectField>
            </FormControl>
            <Button
              className={classes.generateButton}
              fullWidth
              size="small"
              variant="contained"
              onClick={handleSubmit}
              color="primary"
              startIcon={<VpnKeyIcon />}
            >
              Generate New Code
            </Button>
          </Box>
        )}
      />
      {codes.length === 0 ? (
        noOrdersNode
      ) : (
        <List>
          {codes.map(code => (
            <RegistrationCodeListItem key={code.code} item={code} />
          ))}
        </List>
      )}
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  manufacturerAccountData: getAccountData(state),
});

export default compose(connect(mapStateToProps, null))(ManufacturerRegistrationCodes);
