// @flow
import * as React from 'react';
import type { Manufacturer } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { getAllManufacturers } from 'microshopper-platform/services/manufacturer';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import ManufacturerCard from 'microshopper-platform/pages/common/ManufacturerCard';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import { useDispatch } from 'react-redux';

const Manufacturers = (): React.Node => {
  const dispatch = useDispatch();
  const [manufacturers, setManufacturers] = React.useState<Array<Manufacturer>>([]);

  React.useEffect(() => {
    dispatch(showProgressAction());
    getAllManufacturers()
      .then((manufacturerData: Array<Manufacturer>) => {
        setManufacturers(manufacturerData);
      })
      .finally(() => {
        dispatch(hideProgressAction());
      });
  }, [dispatch]);

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Manufacturers" />
      <Grid container spacing={2} direction="row" justify="flex-start" alignItems="flex-start">
        {manufacturers.map(manufacturer => (
          <Grid key={manufacturer.id} item xs={12} sm={4}>
            <ManufacturerCard manufacturer={manufacturer} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Manufacturers;
