// @flow
import * as React from 'react';
import isEmpty from 'lodash/isEmpty';
import Paper from '@material-ui/core/Paper';
import { Form } from 'react-final-form';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import SelectField from 'microshopper-platform/components/final-form/SelectField';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { required } from 'microshopper-platform/utils/validators';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import type { AddProduct, Category, Product } from 'microshopper-platform/api/generated-sources/product/src';
import { connect, useDispatch } from 'react-redux';
import { getAccountData } from 'microshopper-platform/redux/account/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';
import type { ManufacturerAccountData } from 'microshopper-platform/redux/account/types.flow';
import { addProduct, getAllCategories, editProduct } from 'microshopper-platform/services/product';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import type { Location } from 'react-router-dom';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'stretch',
  },
}));

type FormProps = {
  name: string,
  price: number,
  categoryId: number | string,
  description: string,
  quantity: number,
};

type Props = {
  accountData: ManufacturerAccountData,
  location: Location,
};

const AddEditProduct = ({ accountData, location }: Props): React.Node => {
  const classes = useStyles();
  const { showSnackbarMessage } = useSnackbarMessage();
  const dispatch = useDispatch();
  const [isFetching, setIsFetching] = React.useState<boolean>(true);
  const [categories, setCategories] = React.useState<Array<Category>>([]);
  const [isEdit, setIsEdit] = React.useState<boolean>(false);
  const [productToBeEdited, setProductToBeEdited] = React.useState<Product>({});

  React.useEffect(() => {
    dispatch(showProgressAction());
    getAllCategories()
      .then((categoriesData: Array<Category>) => setCategories(categoriesData))
      .finally(() => {
        setIsFetching(false);
        dispatch(hideProgressAction());
      });
    if (!isEmpty(location.state)) {
      setProductToBeEdited(location.state.product);
      setIsEdit(true);
    }
  }, [dispatch, location]);

  const initialValues = React.useMemo<FormProps>((): FormProps => {
    if (!isEdit) {
      return {
        name: '',
        price: 0,
        categoryId: '',
        description: '',
        quantity: 0,
      };
    }
    return {
      name: productToBeEdited.name,
      price: productToBeEdited.price,
      categoryId: productToBeEdited.category.id,
      description: productToBeEdited.description,
      quantity: productToBeEdited.quantity,
    };
  }, [isEdit, productToBeEdited]);

  const handleAddProductClick = React.useCallback(
    (formValues: FormProps) => {
      const product: AddProduct = {
        name: formValues.name,
        price: formValues.price,
        categoryId: formValues.categoryId,
        description: formValues.description,
        quantity: formValues.quantity,
        manufacturerId: accountData.id,
      };
      addProduct(product)
        .then(() => {
          showSnackbarMessage('Add product successful', snackbarMessageType.SUCCESS, false);
          redirectTo('/my-products');
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while adding product.', snackbarMessageType.ERROR, false);
        });
    },
    [accountData, showSnackbarMessage],
  );

  const handleEditProductClick = React.useCallback(
    (formValues: FormProps) => {
      const product: Product = {
        name: formValues.name,
        price: formValues.price,
        categoryId: formValues.categoryId,
        description: formValues.description,
        quantity: formValues.quantity,
        manufacturerId: productToBeEdited.manufacturerId,
      };
      editProduct(productToBeEdited.id, product)
        .then(() => {
          showSnackbarMessage('Edit product successful.', snackbarMessageType.SUCCESS, false);
          redirectTo('/my-products');
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while editing product.', snackbarMessageType.ERROR, false);
        });
    },
    [productToBeEdited, showSnackbarMessage],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  if (isFetching) {
    return null;
  }

  return (
    <Container maxWidth="lg">
      <ApplicationBarTitleProvider currentPageTitle="Add/Edit Product" />
      <Paper className={classes.root}>
        <Form
          initialValues={initialValues}
          onSubmit={isEdit ? handleEditProductClick : handleAddProductClick}
          render={({ handleSubmit }) => (
            <FormControl fullWidth>
              <TextFieldInput
                margin="dense"
                id="name"
                name="name"
                label="Name"
                type="text"
                fullWidth
                validate={validate}
              />
              <TextFieldInput
                margin="dense"
                id="price"
                name="price"
                label="Price"
                type="number"
                fullWidth
                validate={validate}
              />
              <SelectField label="Category" id="categoryId" name="categoryId">
                {categories.map(category => (
                  <MenuItem key={category.id} value={category.id}>
                    {category.name}
                  </MenuItem>
                ))}
              </SelectField>
              <TextFieldInput
                margin="dense"
                id="description"
                name="description"
                label="Description"
                type="text"
                multiline
                rows="5"
                fullWidth
                validate={validate}
              />
              <TextFieldInput
                margin="dense"
                id="quantity"
                name="quantity"
                label="Quantity"
                type="number"
                fullWidth
                validate={validate}
              />
              <Button variant="contained" onClick={handleSubmit} color="primary">
                {isEdit ? 'Edit Product' : 'Add Product'}
              </Button>
            </FormControl>
          )}
        />
      </Paper>
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  accountData: getAccountData(state),
});

export default compose(connect(mapStateToProps, null), withRouter)(AddEditProduct);
