// @flow
import * as React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import type { ShoppingCartItem, DeleteItemAction } from 'microshopper-platform/redux/shopping-cart/types.flow';
import monitorImage from 'microshopper-platform/assets/images/monitor.png';
import { deleteItemAction } from 'microshopper-platform/redux/shopping-cart/slice';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

type Props = {
  item: ShoppingCartItem,
  deleteItemFromCart: Function,
};

const ShoppingCartListItem = ({ item, deleteItemFromCart }: Props): React.Node => {
  const handleDeleteItemFromCart = React.useCallback(() => {
    const action: DeleteItemAction = {
      productId: item.productId,
    };
    deleteItemFromCart(action);
  }, [deleteItemFromCart, item.productId]);

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar variant="square" src={monitorImage} />
      </ListItemAvatar>
      <ListItemText primary={item.productName} secondary={`Price: ${item.productPrice} x ${item.productQuantity}`} />
      <ListItemSecondaryAction>
        <IconButton edge="end" aria-label="Remove from Cart" onClick={() => handleDeleteItemFromCart()}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

const mapDispatchToProps = {
  deleteItemFromCart: deleteItemAction,
};

export default compose(connect(null, mapDispatchToProps))(ShoppingCartListItem);
