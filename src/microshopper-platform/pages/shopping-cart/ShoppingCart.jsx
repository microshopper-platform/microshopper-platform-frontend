// @flow
import * as React from 'react';
import type { ShoppingCartItem } from 'microshopper-platform/redux/shopping-cart/types.flow';
import Container from '@material-ui/core/Container';
import ShoppingCartListItem from 'microshopper-platform/pages/shopping-cart/ShoppingCartListItem';
import type { State } from 'microshopper-platform/redux/types.flow';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { getShoppingCart } from 'microshopper-platform/redux/shopping-cart/selectors';
import { emptyCartAction } from 'microshopper-platform/redux/shopping-cart/slice';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import ShoppingCartFooterSection from 'microshopper-platform/pages/shopping-cart/ShoppingCartFooterSection';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import { showConfirmationDialogAction } from 'microshopper-platform/redux/confirmation-dialog/slice';

const useStyles = makeStyles(() => ({
  divider: {
    margin: '10px 0 10px 0',
  },
}));

type Props = {
  items: Array<ShoppingCartItem>,
  emptyCart: Function,
  showConfirmationDialog: Function,
};

const ShoppingCart = ({ items, emptyCart, showConfirmationDialog }: Props): React.Node => {
  const classes = useStyles();
  const [isCartEmpty, setIsCartEmpty] = React.useState<boolean>(true);
  const [totalAmount, setTotalAmount] = React.useState<number>(0);
  React.useEffect(() => {
    if (items.length > 0) {
      setTotalAmount(items.map(item => item.productPrice * item.productQuantity).reduce((a, b) => a + b, 0));
      setIsCartEmpty(false);
    } else {
      setTotalAmount(0);
    }
  }, [items, setIsCartEmpty, setTotalAmount]);

  const emptyCartNode = (
    <Typography align="center" variant="h5">
      Your shopping cart is empty.
    </Typography>
  );

  return (
    <Container maxWidth="lg">
      <ApplicationBarTitleProvider currentPageTitle="Shopping Cart" />
      {isCartEmpty ? (
        emptyCartNode
      ) : (
        <List dense>
          {items.map(item => (
            <ShoppingCartListItem key={item.productId} item={item} />
          ))}
        </List>
      )}
      <Divider className={classes.divider} />
      <ShoppingCartFooterSection
        isCartEmpty={isCartEmpty}
        emptyCart={emptyCart}
        totalAmount={totalAmount}
        showConfirmationDialog={showConfirmationDialog}
      />
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  items: getShoppingCart(state),
});

const mapDispatchToProps = {
  emptyCart: emptyCartAction,
  showConfirmationDialog: showConfirmationDialogAction,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(ShoppingCart);
