// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { Link } from 'react-router-dom';
import type { ShowConfirmationDialogAction } from 'microshopper-platform/redux/confirmation-dialog/types.flow';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

type Props = {
  emptyCart: Function,
  totalAmount: number,
  isCartEmpty: boolean,
  showConfirmationDialog: Function,
};

const ShoppingCartFooterSection = ({
  emptyCart,
  totalAmount,
  isCartEmpty,
  showConfirmationDialog,
}: Props): React.Node => {
  const classes = useStyles();

  const showConfirmationDialogCallback = React.useCallback(() => {
    const action: ShowConfirmationDialogAction = {
      title: 'Are you sure you want to empty your shopping cart?',
      message: 'This action is irreversible!',
      callback: emptyCart,
    };
    showConfirmationDialog(action);
  }, [emptyCart, showConfirmationDialog]);

  return (
    <>
      <Box component="div" className={classes.root} display="flex" flexDirection="row">
        <Box component="div" flexGrow={1}>
          <Typography align="left" variant="h6" className={classes.margin}>
            Total price: {totalAmount}
          </Typography>
        </Box>
        <Box component="div">
          <Button
            variant="contained"
            disabled={isCartEmpty}
            className={classes.margin}
            color="primary"
            size="small"
            startIcon={<DeleteForeverIcon />}
            onClick={() => showConfirmationDialogCallback()}
          >
            Empty Cart
          </Button>
          <Button
            component={Link}
            to="/checkout"
            disabled={isCartEmpty}
            className={classes.margin}
            color="primary"
            variant="contained"
            startIcon={<CheckIcon />}
          >
            Proceed to Checkout
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default ShoppingCartFooterSection;
