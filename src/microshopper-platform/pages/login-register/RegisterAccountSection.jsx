// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Form } from 'react-final-form';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { required } from 'microshopper-platform/utils/validators';
import { registerAccount } from 'microshopper-platform/services/account';
import type { AccountRegistration } from 'microshopper-platform/api/generated-sources/account/src';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';

const useStyles = makeStyles(() => ({
  generalMargin: {
    margin: 10,
  },
}));

type FormProps = {
  username: string,
  password: string,
  email: string,
  firstName: string,
  lastName: string,
};

const RegisterAccountSection = (): React.Node => {
  const classes = useStyles();
  const { showSnackbarMessage } = useSnackbarMessage();

  const initialValues = React.useMemo<FormProps>(
    (): FormProps => ({
      username: '',
      password: '',
      email: '',
      firstName: '',
      lastName: '',
    }),
    [],
  );

  const handleRegisterClick = React.useCallback(
    (formValues: FormProps) => {
      const accountRegistration: AccountRegistration = {
        username: formValues.username,
        password: formValues.password,
        email: formValues.email,
        data: {
          firstName: formValues.firstName,
          lastName: formValues.lastName,
        },
      };
      registerAccount(accountRegistration)
        .then(() => {
          showSnackbarMessage('Account registration successful.', snackbarMessageType.SUCCESS, false);
          redirectTo('/');
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while trying to register account.', snackbarMessageType.ERROR, false);
        });
    },
    [showSnackbarMessage],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  return (
    <Box component="div">
      <Typography align="center" variant="h5">
        Register an Account
      </Typography>
      <Form
        initialValues={initialValues}
        onSubmit={handleRegisterClick}
        render={({ handleSubmit }) => (
          <FormControl fullWidth margin="dense">
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="accountUsername"
              name="username"
              label="Username"
              type="text"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="accountPassword"
              name="password"
              label="Password"
              type="password"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="accountEmail"
              name="email"
              label="Email"
              type="email"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="firstName"
              name="firstName"
              label="First Name"
              type="text"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="lastName"
              name="lastName"
              label="Last Name"
              type="text"
              validate={validate}
            />
            <Button className={classes.generalMargin} variant="contained" onClick={handleSubmit} color="primary">
              Register
            </Button>
          </FormControl>
        )}
      />
    </Box>
  );
};

export default RegisterAccountSection;
