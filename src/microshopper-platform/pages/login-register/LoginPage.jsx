// @flow
import * as React from 'react';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import { Form } from 'react-final-form';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Link } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { loginAccountAction } from 'microshopper-platform/redux/account/slice';
import type { LoginAccountAction } from 'microshopper-platform/redux/account/types.flow';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';
import { required } from 'microshopper-platform/utils/validators';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';

const useStyles = makeStyles(() => ({
  generalMargin: {
    margin: 10,
  },
}));

type FormProps = {
  username: string,
  password: string,
};

type Props = {
  loginAccount: Function,
};

const linkStyle = {
  color: 'black',
};

const LoginPage = ({ loginAccount }: Props): React.Node => {
  const classes = useStyles();
  const initialValues = React.useMemo<FormProps>(
    (): FormProps => ({
      username: '',
      password: '',
    }),
    [],
  );

  const handleLoginClick = React.useCallback(
    (formValues: FormProps) => {
      const action: LoginAccountAction = {
        username: formValues.username,
        password: formValues.password,
      };
      loginAccount(action);
    },
    [loginAccount],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  return (
    <Container maxWidth="md">
      <ApplicationBarTitleProvider currentPageTitle="Login" />
      <Paper>
        <Form
          initialValues={initialValues}
          onSubmit={handleLoginClick}
          render={({ handleSubmit }) => (
            <FormControl fullWidth margin="dense">
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="username"
                name="username"
                label="Username"
                type="text"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="password"
                name="password"
                label="Password"
                type="password"
                validate={validate}
              />
              <Button className={classes.generalMargin} variant="contained" onClick={handleSubmit} color="primary">
                Login
              </Button>
            </FormControl>
          )}
        />
        <Box component="div" className={classes.generalMargin} display="flex" flexDirection="row-reverse">
          <Link to="/register" style={linkStyle}>
            Dont have an account? Register
          </Link>
        </Box>
      </Paper>
    </Container>
  );
};

const mapDispatchToProps = {
  loginAccount: loginAccountAction,
  showSnackbarMessage: showSnackbarMessageAction,
};

export default compose(connect(null, mapDispatchToProps))(LoginPage);
