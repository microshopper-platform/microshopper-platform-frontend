// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Form } from 'react-final-form';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import { required } from 'microshopper-platform/utils/validators';
import type { ManufacturerRegistration } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { registerManufacturer } from 'microshopper-platform/services/manufacturer';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';

const useStyles = makeStyles(() => ({
  generalMargin: {
    margin: 10,
  },
}));

type FormProps = {
  username: string,
  password: string,
  email: string,
  name: string,
};

const RegisterManufacturerSection = (): React.Node => {
  const classes = useStyles();
  const { showSnackbarMessage } = useSnackbarMessage();

  const initialValues = React.useMemo<FormProps>(
    (): FormProps => ({
      username: '',
      password: '',
      email: '',
      name: '',
    }),
    [],
  );

  const handleRegisterClick = React.useCallback(
    (formValues: FormProps) => {
      const manufacturerRegistration: ManufacturerRegistration = {
        manufacturerAccountRegistration: {
          username: formValues.username,
          password: formValues.password,
          email: formValues.email,
        },
        manufacturer: {
          name: formValues.name,
        },
      };
      registerManufacturer(manufacturerRegistration)
        .then(() => {
          showSnackbarMessage('Manufacturer registration successful.', snackbarMessageType.SUCCESS, false);
          redirectTo('/');
        })
        .catch(() => {
          showSnackbarMessage(
            'Error occurred while trying to register manufacturer.',
            snackbarMessageType.ERROR,
            false,
          );
        });
    },
    [showSnackbarMessage],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  return (
    <Box component="div">
      <Typography align="center" variant="h5">
        Register an Manufacturer
      </Typography>
      <Form
        initialValues={initialValues}
        onSubmit={handleRegisterClick}
        render={({ handleSubmit }) => (
          <FormControl fullWidth margin="dense">
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="manufacturerAccountUsername"
              name="username"
              label="Username"
              type="text"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="manufacturerAccountPassword"
              name="password"
              label="Password"
              type="password"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="manufacturerAccountEmail"
              name="email"
              label="Email"
              type="email"
              validate={validate}
            />
            <TextFieldInput
              className={classes.generalMargin}
              margin="dense"
              id="manufacturerName"
              name="name"
              label="Manufacturer name"
              type="text"
              validate={validate}
            />
            <Button className={classes.generalMargin} variant="contained" onClick={handleSubmit} color="primary">
              Register
            </Button>
          </FormControl>
        )}
      />
    </Box>
  );
};

export default RegisterManufacturerSection;
