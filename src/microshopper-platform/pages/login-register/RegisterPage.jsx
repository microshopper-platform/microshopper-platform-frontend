// @flow
import * as React from 'react';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import RegisterAccountSection from 'microshopper-platform/pages/login-register/RegisterAccountSection';
import TabSection from 'microshopper-platform/components/common/TabSection';
import RegisterManufacturerSection from 'microshopper-platform/pages/login-register/RegisterManufacturerSection';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import RegisterManufacturerAccountSection from 'microshopper-platform/pages/login-register/RegisterManufacturerAccountSection';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles({
  root: {
    justifyContent: 'center',
  },
  scroller: {
    flexGrow: '0',
  },
});

const RegisterPage = (): React.Node => {
  const classes = useStyles();
  const [tabIndex, setTabIndex] = React.useState<number>(0);

  const handleTabChange = React.useCallback((event: Object, newValue: number) => {
    setTabIndex(newValue);
  }, []);

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Register" />
      <Paper>
        <Tabs
          classes={{ root: classes.root, scroller: classes.scroller }}
          variant="scrollable"
          scrollButtons="auto"
          value={tabIndex}
          onChange={handleTabChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="Account" />
          <Tab label="Register Manufacturer" />
          <Tab label="Register Manufacturer Account" />
        </Tabs>
        <TabSection index={0} currentIndex={tabIndex}>
          <RegisterAccountSection />
        </TabSection>
        <TabSection index={1} currentIndex={tabIndex}>
          <RegisterManufacturerSection />
        </TabSection>
        <TabSection index={2} currentIndex={tabIndex}>
          <RegisterManufacturerAccountSection />
        </TabSection>
      </Paper>
    </Container>
  );
};

export default RegisterPage;
