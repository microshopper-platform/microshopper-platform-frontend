// @flow
import * as React from 'react';
import FormControl from '@material-ui/core/FormControl';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';

type Props = {
  validate: Function,
};

const AccountDataSection = ({ validate }: Props): React.Node => {
  return (
    <FormControl fullWidth margin="dense">
      <TextFieldInput
        margin="dense"
        id="firstName"
        name="firstName"
        label="First Name"
        type="text"
        fullWidth
        validate={validate}
      />
      <TextFieldInput
        margin="dense"
        id="lastName"
        name="lastName"
        label="Last Name"
        type="text"
        fullWidth
        validate={validate}
      />
      <TextFieldInput
        margin="dense"
        id="country"
        name="country"
        label="Country"
        type="text"
        fullWidth
        validate={validate}
      />
      <TextFieldInput margin="dense" id="city" name="city" label="City" type="text" fullWidth validate={validate} />
      <TextFieldInput
        margin="dense"
        id="address"
        name="address"
        label="Address"
        type="text"
        fullWidth
        validate={validate}
      />
    </FormControl>
  );
};

export default AccountDataSection;
