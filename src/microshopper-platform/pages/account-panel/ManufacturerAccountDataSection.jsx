// @flow
import * as React from 'react';
import FormControl from '@material-ui/core/FormControl';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import type { Role } from 'microshopper-platform/redux/account/types.flow';
import { isManufacturerTransporter } from 'microshopper-platform/utils/roleUtils';

type Props = {
  validate: Function,
  manufacturerRole: Role,
};

const ManufacturerAccountDataSection = ({ validate, manufacturerRole }: Props): React.Node => (
  <FormControl fullWidth margin="dense">
    <TextFieldInput disabled hidden margin="dense" id="id" name="id" label="ID" type="number" fullWidth />
    <TextFieldInput
      disabled={isManufacturerTransporter(manufacturerRole)}
      margin="dense"
      id="name"
      name="name"
      label="Name"
      type="text"
      fullWidth
      validate={validate}
    />
    <TextFieldInput
      disabled={isManufacturerTransporter(manufacturerRole)}
      margin="dense"
      id="description"
      name="description"
      label="Description"
      type="text"
      fullWidth
      validate={validate}
    />
  </FormControl>
);

export default ManufacturerAccountDataSection;
