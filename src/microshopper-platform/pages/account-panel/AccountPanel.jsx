// @flow
import * as React from 'react';
import type {
  AbstractAccount,
  AccountData,
  ManufacturerAccountData,
  UpdateAccountDataAction,
  UpdateManufacturerAccountDataAction,
} from 'microshopper-platform/redux/account/types.flow';
import makeStyles from '@material-ui/core/styles/makeStyles';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import { Form } from 'react-final-form';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import avatarImage from 'microshopper-platform/assets/images/avatar.png';
import { connect } from 'react-redux';
import { getAccount, getAccountData } from 'microshopper-platform/redux/account/selectors';
import { updateAccountDataAction } from 'microshopper-platform/redux/account/slice';
import AccountDataSection from 'microshopper-platform/pages/account-panel/AccountDataSection';
import ManufacturerAccountDataSection from 'microshopper-platform/pages/account-panel/ManufacturerAccountDataSection';
import type { State } from 'microshopper-platform/redux/types.flow';
import { required } from 'microshopper-platform/utils/validators';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import { compose } from 'redux';
import { isUserRole, isManufacturerRole } from 'microshopper-platform/utils/roleUtils';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'stretch',
  },
  accountSection: {
    padding: theme.spacing(3, 2),
    display: 'flex',
    flexDirection: 'row',
  },
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
  dataSection: {
    padding: theme.spacing(3, 2),
  },
  generalMargin: {
    margin: 10,
  },
}));

type Props = {
  account: AbstractAccount,
  data: AccountData & ManufacturerAccountData,
  updateAccountData: Function,
};

const AccountPanel = ({ account, data, updateAccountData }: Props): React.Node => {
  const classes = useStyles();

  const initialValues = React.useMemo<Object>((): Object => {
    const initialValues_ = {
      username: account.username,
      email: account.email,
    };
    if (isUserRole(account.role)) {
      return {
        ...initialValues_,
        firstName: data.firstName,
        lastName: data.lastName,
        country: data.country,
        city: data.city,
        address: data.address,
      };
    }
    if (isManufacturerRole(account.role)) {
      return {
        ...initialValues_,
        id: data.id,
        name: data.name,
        description: data.description,
      };
    }
  }, [account, data]);

  const handleUpdateClick = React.useCallback(
    (formValues: UpdateAccountDataAction | UpdateManufacturerAccountDataAction) => {
      updateAccountData(formValues);
    },
    [updateAccountData],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  return (
    <Container maxWidth="md">
      <ApplicationBarTitleProvider currentPageTitle="Account Panel" />
      <Paper>
        <Form
          initialValues={initialValues}
          onSubmit={handleUpdateClick}
          render={({ handleSubmit }) => (
            <Box component="div" className={classes.root}>
              <Box component="div" className={classes.accountSection}>
                <Box component="div">
                  <img src={avatarImage} alt="product_image" className={classes.image} />
                </Box>
                <FormControl fullWidth margin="dense">
                  <TextFieldInput
                    disabled
                    margin="dense"
                    id="username"
                    name="username"
                    label="Username"
                    type="text"
                    validate={validate}
                  />
                  <TextFieldInput
                    margin="dense"
                    id="email"
                    name="email"
                    label="Email"
                    type="text"
                    validate={validate}
                  />
                </FormControl>
              </Box>
              <Box component="div" className={classes.dataSection}>
                {isUserRole(account.role) ? (
                  <AccountDataSection validate={validate} />
                ) : (
                  <ManufacturerAccountDataSection manufacturerRole={account.role} validate={validate} />
                )}
              </Box>
              <Button className={classes.generalMargin} variant="contained" onClick={handleSubmit} color="primary">
                Update
              </Button>
            </Box>
          )}
        />
      </Paper>
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  account: getAccount(state),
  data: getAccountData(state),
});

const mapDispatchToProps = {
  updateAccountData: updateAccountDataAction,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(AccountPanel);
