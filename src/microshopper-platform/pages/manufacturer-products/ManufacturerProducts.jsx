// @flow
import * as React from 'react';
import isEmpty from 'lodash/isEmpty';
import type { Product } from 'microshopper-platform/api/generated-sources/product/src';
import { getAllManufacturerProducts, deleteProduct } from 'microshopper-platform/services/product';
import Container from '@material-ui/core/Container';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import { connect, useDispatch } from 'react-redux';
import type { State } from 'microshopper-platform/redux/types.flow';
import { getAccountData } from 'microshopper-platform/redux/account/selectors';
import type { ManufacturerAccountData } from 'microshopper-platform/redux/account/types.flow';
import MaterialTableWrapper from 'microshopper-platform/components/common/MaterialTableWrapper';
import { compose } from 'redux';
import Edit from '@material-ui/icons/Edit';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import { showConfirmationDialogAction } from 'microshopper-platform/redux/confirmation-dialog/slice';
import type { ShowConfirmationDialogAction } from 'microshopper-platform/redux/confirmation-dialog/types.flow';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { redirectToWithState } from 'microshopper-platform/utils/historyUtils';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';

type Props = {
  accountData: ManufacturerAccountData,
  showConfirmationDialog: Function,
};

const ManufacturerProducts = ({ accountData, showConfirmationDialog }: Props): React.Node => {
  const { showSnackbarMessage } = useSnackbarMessage();
  const dispatch = useDispatch();
  const [products, setProducts] = React.useState<Array<Product>>([]);

  React.useEffect(() => {
    if (!isEmpty(accountData)) {
      dispatch(showProgressAction());
      getAllManufacturerProducts(accountData.id)
        .then((productsData: Array<Product>) => {
          setProducts(productsData);
        })
        .finally(() => {
          dispatch(hideProgressAction());
        });
    }
  }, [accountData, dispatch]);

  const editProductCallback = React.useCallback((rowData: Product) => {
    redirectToWithState({
      pathname: '/add-product',
      state: { product: rowData },
    });
  }, []);

  const deleteProductClickCallback = React.useCallback(
    (rowData: Product) => {
      const deleteProductCallback = () => {
        deleteProduct(rowData.id)
          .then(() => {
            showSnackbarMessage('Delete product successful.', snackbarMessageType.SUCCESS, false);
          })
          .catch(() => {
            showSnackbarMessage('Error occurred while deleting product.', snackbarMessageType.ERROR, false);
          });
      };

      const action: ShowConfirmationDialogAction = {
        title: 'Are you sure you want to delete the product?',
        message: 'This action is irreversible!',
        callback: deleteProductCallback,
      };
      showConfirmationDialog(action);
    },
    [showConfirmationDialog, showSnackbarMessage],
  );

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="My Products" />
      <MaterialTableWrapper
        columns={[
          { title: 'Name', field: 'name' },
          { title: 'Price', field: 'price', type: 'numeric' },
          { title: 'Category', field: 'category.name' },
          { title: 'Quantity', field: 'quantity', type: 'numeric' },
        ]}
        data={products}
        title="Your Products"
        actions={[
          {
            icon: Edit,
            tooltip: 'Edit Product',
            onClick: (event: Object, rowData: Product) => {
              editProductCallback(rowData);
            },
          },
          {
            icon: DeleteOutline,
            tooltip: 'Delete Product',
            onClick: (event: Object, rowData: Product) => {
              deleteProductClickCallback(rowData);
            },
          },
        ]}
      />
    </Container>
  );
};

const mapDispatchToProps = {
  showConfirmationDialog: showConfirmationDialogAction,
};

const mapStateToProps = (state: State) => ({
  accountData: getAccountData(state),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(ManufacturerProducts);
