// @flow
import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import type { Product } from 'microshopper-platform/api/generated-sources/product-catalog/src';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import monitorImage from 'microshopper-platform/assets/images/monitor.png';
import type {
  ProductReview,
  ProductReviewRatingEnum,
} from 'microshopper-platform/api/generated-sources/product-review/src';
import { Form } from 'react-final-form';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import SelectField from 'microshopper-platform/components/final-form/SelectField';
import MenuItem from '@material-ui/core/MenuItem';
import { addProductReview } from 'microshopper-platform/services/product-review';
import Button from '@material-ui/core/Button';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import makeStyles from '@material-ui/core/styles/makeStyles';
import FormControl from '@material-ui/core/FormControl';
import { required } from 'microshopper-platform/utils/validators';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import { connect } from 'react-redux';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { getAccount } from 'microshopper-platform/redux/account/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';
import type { AbstractAccount } from 'microshopper-platform/redux/account/types.flow';
import type { Location } from 'react-router-dom';
import { useSnackbarMessage } from 'microshopper-platform/redux/snackbar-message/hook';
import Grid from '@material-ui/core/Grid';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';

const useStyles = makeStyles(() => ({
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
}));

type Props = {
  location: Location,
  account: AbstractAccount,
};

type FormProps = {
  title: string,
  rating: ProductReviewRatingEnum,
  description: string,
};

const AddProductReview = ({ location, account }: Props): React.Node => {
  const classes = useStyles();
  const { showSnackbarMessage } = useSnackbarMessage();
  const { product }: { product: Product } = location.state;

  const initialValues = React.useMemo<FormProps>(
    (): FormProps => ({
      title: '',
      rating: 'EXCELLENT',
      description: '',
    }),
    [],
  );

  const handleAddReviewClick = React.useCallback(
    (formValues: FormProps) => {
      const addProductReviewData: ProductReview = {
        ...formValues,
        productId: product.id,
        username: account.username,
      };
      addProductReview(addProductReviewData)
        .then(() => {
          showSnackbarMessage('Successfully added product review', snackbarMessageType.SUCCESS, false);
          redirectTo(`/product/${product.id}`);
        })
        .catch(() => {
          showSnackbarMessage('Error occurred while trying to add product review', snackbarMessageType.ERROR, false);
        });
    },
    [account, product.id, showSnackbarMessage],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Add Product Review" />
      <Paper>
        <Grid
          container
          spacing={2}
          direction="row"
          justify="space-evenly"
          alignItems="center"
          alignContent="space-around"
        >
          <Grid item xs={12} sm={4}>
            <Box component="div">
              <Typography variant="h5" component="h3">
                Add Review for {product.name}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Box component="div">
              <img src={monitorImage} alt="product_image" className={classes.image} />
            </Box>
          </Grid>
        </Grid>
      </Paper>
      <Form
        initialValues={initialValues}
        onSubmit={handleAddReviewClick}
        render={({ handleSubmit }) => (
          <FormControl fullWidth margin="dense">
            <TextFieldInput
              margin="dense"
              id="title"
              name="title"
              label="Title"
              type="text"
              fullWidth
              validate={validate}
            />
            <SelectField name="rating" label="Select rating" autoWidth>
              <MenuItem value="EXCELLENT">Excellent</MenuItem>
              <MenuItem value="VERY_GOOD">Very Good</MenuItem>
              <MenuItem value="GOOD">Good</MenuItem>
              <MenuItem value="BAD">Bad</MenuItem>
              <MenuItem value="VERY_BAD">Very Bad</MenuItem>
            </SelectField>
            <TextFieldInput
              margin="dense"
              id="description"
              name="description"
              label="Description"
              type="text"
              multiline
              rows="5"
              fullWidth
              validate={validate}
            />
            <Button variant="contained" onClick={handleSubmit} color="primary">
              Add Review
            </Button>
          </FormControl>
        )}
      />
    </Container>
  );
};

const mapStateToProps = (state: State) => ({
  account: getAccount(state),
});

export default compose(connect(mapStateToProps, null), withRouter)(AddProductReview);
