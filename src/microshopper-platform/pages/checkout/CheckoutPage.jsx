// @flow
import * as React from 'react';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import { Form } from 'react-final-form';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import type { AccountData } from 'microshopper-platform/redux/account/types.flow';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { required } from 'microshopper-platform/utils/validators';
import { connect } from 'react-redux';
import { checkoutCartAction } from 'microshopper-platform/redux/shopping-cart/slice';
import { getAccountData } from 'microshopper-platform/redux/account/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';
import { compose } from 'redux';

const useStyles = makeStyles(() => ({
  generalMargin: {
    margin: 10,
  },
}));

type FormProps = {
  firstName: string,
  lastName: string,
  country: string,
  city: string,
  billingAddress: string,
  creditCardNumber: string,
  creditCardSecurityNumber: string,
};

type Props = {
  accountData: AccountData,
  checkoutCart: Function,
};

const CheckoutPage = ({ accountData, checkoutCart }: Props): React.Node => {
  const classes = useStyles();
  const initialValues = React.useMemo<FormProps>(
    (): FormProps => ({
      firstName: accountData.firstName,
      lastName: accountData.lastName,
      country: accountData.country,
      city: accountData.city,
      billingAddress: accountData.address,
      creditCardNumber: '123-456-789',
      creditCardSecurityNumber: '999',
    }),
    [accountData],
  );

  const handlePurchaseClick = React.useCallback(
    (formValues: FormProps) => {
      checkoutCart(formValues);
    },
    [checkoutCart],
  );

  const validate = React.useCallback<any>((value: any): any => required('Required')(value), []);

  return (
    <Container>
      <ApplicationBarTitleProvider currentPageTitle="Checkout" />
      <Typography align="center" variant="h5">
        Checkout
      </Typography>
      <Paper>
        <Form
          initialValues={initialValues}
          onSubmit={handlePurchaseClick}
          render={({ handleSubmit }) => (
            <FormControl fullWidth margin="dense">
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="firstName"
                name="firstName"
                label="First Name"
                type="text"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="lastName"
                name="lastName"
                label="Last Name"
                type="text"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="country"
                name="country"
                label="Country"
                type="email"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="city"
                name="city"
                label="City"
                type="text"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="billingAddress"
                name="billingAddress"
                label="Billing Address"
                type="text"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="creditCardNumber"
                name="creditCardNumber"
                label="Credit Card Number"
                type="text"
                validate={validate}
              />
              <TextFieldInput
                className={classes.generalMargin}
                margin="dense"
                id="creditCardSecurityNumber"
                name="creditCardSecurityNumber"
                label="Credit Card Security Number"
                type="text"
                validate={validate}
              />
              <Button className={classes.generalMargin} variant="contained" onClick={handleSubmit} color="primary">
                Purchase
              </Button>
            </FormControl>
          )}
        />
      </Paper>
    </Container>
  );
};

const mapDispatchToProps = {
  checkoutCart: checkoutCartAction,
};

const mapStateToProps = (state: State) => ({
  accountData: getAccountData(state),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(CheckoutPage);
