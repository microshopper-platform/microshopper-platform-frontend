// @flow
import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import { Form } from 'react-final-form';
import TextFieldInput from 'microshopper-platform/components/final-form/TextFieldInput';
import { composeValidators, required, maxValue, minValue } from 'microshopper-platform/utils/validators';

type FormProps = {
  desiredQuantity: number,
};

type Props = {
  addToCartCallback: Function,
  handleDialogState: Function,
  open: boolean,
  productName: string,
  productQuantity: number,
  oldDesiredQuantity: number,
};

const AddToShoppingCartDialog = ({
  addToCartCallback,
  handleDialogState,
  open,
  productName,
  productQuantity,
  oldDesiredQuantity,
}: Props): React.Node => {
  const initialValues = React.useMemo<FormProps>(
    (): FormProps => ({
      desiredQuantity: oldDesiredQuantity,
    }),
    [oldDesiredQuantity],
  );

  const validate = React.useCallback<any>(
    (value: any): any =>
      composeValidators(
        maxValue(`Desired quantity cannot be greater than ${productQuantity}`, productQuantity),
        minValue('Desired quantity cannot be less than 1', 1),
        required('Required'),
      )(value),
    [productQuantity],
  );

  const handleAddToCartClick = React.useCallback(
    (formValues: FormProps) => {
      addToCartCallback(formValues.desiredQuantity);
    },
    [addToCartCallback],
  );

  return (
    <Box component="div">
      <Dialog open={open} onClose={() => handleDialogState(false)} aria-labelledby="form-dialog-title">
        <Form
          initialValues={initialValues}
          onSubmit={handleAddToCartClick}
          render={({ handleSubmit }) => (
            <>
              <DialogTitle id="form-dialog-title">Add {productName} to Shopping Cart</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  To add {productName} to shopping cart, first please choose your desired quantity:
                </DialogContentText>
                <FormControl fullWidth margin="dense">
                  <TextFieldInput
                    fullWidth
                    margin="dense"
                    id="desiredQuantity"
                    name="desiredQuantity"
                    label="Desired Quantity"
                    type="number"
                    validate={validate}
                  />
                </FormControl>
              </DialogContent>
              <DialogActions>
                <Button onClick={() => handleDialogState(false)} color="primary">
                  Cancel
                </Button>
                <Button variant="contained" onClick={handleSubmit} color="primary">
                  Add to Cart
                </Button>
              </DialogActions>
            </>
          )}
        />
      </Dialog>
    </Box>
  );
};

export default AddToShoppingCartDialog;
