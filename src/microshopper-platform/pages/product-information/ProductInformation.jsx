// @flow
import * as React from 'react';
import { withRouter } from 'react-router';
import type { CompleteProductInformation } from 'microshopper-platform/api/generated-sources/product-catalog/src';
import { getCompleteProductInformation } from 'microshopper-platform/services/product-catalog';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AdditionalInformationSection from 'microshopper-platform/pages/product-information/AdditionalInformationSection';
import BaseInformationSection from 'microshopper-platform/pages/product-information/BaseInformationSection';
import TabSection from 'microshopper-platform/components/common/TabSection';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import RateReviewIcon from '@material-ui/icons/RateReview';
import makeStyles from '@material-ui/core/styles/makeStyles';
import ReviewsSection from 'microshopper-platform/pages/product-information/ReviewsSection';
import { compose } from 'redux';
import { connect, useDispatch } from 'react-redux';
import { isProductInCart, getItemQuantity } from 'microshopper-platform/redux/shopping-cart/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';
import { addItemAction, updateItemAction } from 'microshopper-platform/redux/shopping-cart/slice';
import type { AddItemAction, UpdateItemAction } from 'microshopper-platform/redux/shopping-cart/types.flow';
import AddToShoppingCartDialog from 'microshopper-platform/pages/product-information/AddToShoppingCartDialog';
import { Link } from 'react-router-dom';
import { getAccountRole } from 'microshopper-platform/redux/account/selectors';
import type { Role } from 'microshopper-platform/redux/account/types.flow';
import ApplicationBarTitleProvider from 'microshopper-platform/components/common/ApplicationBarTitleProvider';
import DescriptionIcon from '@material-ui/icons/Description';
import InfoIcon from '@material-ui/icons/Info';
import type { Match } from 'react-router-dom';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';
import { hideProgressAction, showProgressAction } from 'microshopper-platform/redux/meta-data/slice';
import { isUserRole } from 'microshopper-platform/utils/roleUtils';

const useStyles = makeStyles(theme => ({
  extendedIcon: {
    paddingRight: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

type Props = {
  addItem: Function,
  updateItem: Function,
  productInCart: boolean,
  match: Match,
  quantityProductInCart: number,
  accountRole: Role,
};

const ProductInformation = ({
  addItem,
  updateItem,
  productInCart,
  match,
  quantityProductInCart,
  accountRole,
}: Props): React.Node => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { productId } = match.params;
  const [product, setProduct] = React.useState<CompleteProductInformation>({});
  const [isFetched, setIsFetched] = React.useState<boolean>(false);
  const [tabIndex, setTabIndex] = React.useState<number>(0);
  const [showAddToShoppingCartDialog, setShowAddToShoppingCartDialog] = React.useState<boolean>(false);

  React.useEffect(() => {
    dispatch(showProgressAction());
    getCompleteProductInformation(productId)
      .then((productData: CompleteProductInformation) => {
        setProduct(productData);
        setIsFetched(true);
      })
      .finally(() => {
        dispatch(hideProgressAction());
      });
  }, [dispatch, productId]);

  const handleAddUpdateItem = React.useCallback(
    (desiredQuantity: number) => {
      if (productInCart) {
        const action: UpdateItemAction = {
          productId: product.data.id,
          productQuantity: desiredQuantity,
        };
        updateItem(action);
      } else {
        const action: AddItemAction = {
          productId: product.data.id,
          manufacturerId: product.manufacturer.id,
          productName: product.data.name,
          productPrice: product.data.price,
          productQuantity: desiredQuantity,
        };
        addItem(action);
      }
      setShowAddToShoppingCartDialog(false);
    },
    [addItem, product, updateItem, productInCart],
  );

  const handleTabChange = React.useCallback((event: Object, newValue: number) => {
    setTabIndex(newValue);
  }, []);

  const cartButtonText = React.useMemo<string>((): string => (productInCart ? 'Update Item in Cart' : 'Add to Cart'), [
    productInCart,
  ]);

  const handleCartButtonClick = React.useCallback(() => {
    if (isUserRole(accountRole)) {
      setShowAddToShoppingCartDialog(true);
    } else {
      redirectTo('/login');
    }
  }, [accountRole]);

  if (!isFetched) {
    return null;
  }

  return (
    <Container maxWidth="xl">
      <ApplicationBarTitleProvider currentPageTitle="Product Information" />
      <Paper>
        <Tabs value={tabIndex} onChange={handleTabChange} indicatorColor="primary" textColor="primary" centered>
          <Tab icon={<InfoIcon />} label="Product" />
          <Tab icon={<DescriptionIcon />} label="Description" />
          <Tab icon={<RateReviewIcon />} label="Reviews" />
        </Tabs>
        <TabSection index={0} currentIndex={tabIndex}>
          <BaseInformationSection
            name={product.data.name}
            price={product.data.price}
            quantity={product.data.quantity}
            manufacturerName={product.manufacturer.name}
          />
          <ButtonGroup variant="contained" color="primary" className={classes.margin}>
            {isUserRole(accountRole) && (
              <Button startIcon={<AddShoppingCartIcon />} onClick={() => handleCartButtonClick()}>
                {cartButtonText}
              </Button>
            )}
            <Button
              startIcon={<RateReviewIcon />}
              component={Link}
              to={{ pathname: '/add-product-review', state: { product: product.data } }}
            >
              Add Review
            </Button>
          </ButtonGroup>
        </TabSection>
        <TabSection index={1} currentIndex={tabIndex}>
          <AdditionalInformationSection
            productDescription={product.data.description}
            manufacturerDescription={product.manufacturer.description}
          />
        </TabSection>
        <TabSection index={2} currentIndex={tabIndex}>
          <ReviewsSection productReviews={product.reviews} />
        </TabSection>
      </Paper>
      <AddToShoppingCartDialog
        addToCartCallback={handleAddUpdateItem}
        open={showAddToShoppingCartDialog}
        handleDialogState={setShowAddToShoppingCartDialog}
        productName={product.data.name}
        productQuantity={product.data.quantity}
        oldDesiredQuantity={quantityProductInCart}
      />
    </Container>
  );
};

const mapStateToProps = (state: State, ownProps: Props) => ({
  productInCart: isProductInCart(state, parseInt(ownProps.match.params.productId, 10)),
  quantityProductInCart: getItemQuantity(state, parseInt(ownProps.match.params.productId, 10)),
  accountRole: getAccountRole(state),
});

const mapDispatchToProps = {
  addItem: addItemAction,
  updateItem: updateItemAction,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withRouter)(ProductInformation);
