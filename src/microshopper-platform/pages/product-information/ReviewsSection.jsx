// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import avatarImage from 'microshopper-platform/assets/images/avatar.png';
import Badge from '@material-ui/core/Badge';
import withStyles from '@material-ui/core/styles/withStyles';
import { mapRatingsToBadgeProperties } from 'microshopper-platform/utils/constants';
import type { ProductReview } from 'microshopper-platform/api/generated-sources/product-catalog/src';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto',
    maxHeight: 500,
    '&::-webkit-scrollbar': {
      width: '0.4em',
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
      outline: '1px solid slategrey',
    },
  },
  listItem: {
    paddingLeft: '35px',
  },
  inline: {
    display: 'inline',
  },
}));

const StyledBadge = withStyles(theme => ({
  badge: {
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
    whiteSpace: 'nowrap',
  },
}))(Badge);

type Props = {
  productReviews: Array<ProductReview>,
};

const ReviewsSection = ({ productReviews }: Props): React.Node => {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      {productReviews.map(productReview => (
        <ListItem
          key={productReview.id}
          divider={productReview !== productReviews[productReviews.length - 1]}
          className={classes.listItem}
          alignItems="flex-start"
        >
          <ListItemAvatar>
            <StyledBadge
              badgeContent={mapRatingsToBadgeProperties.get(productReview.rating).text}
              anchorOrigin={{
                horizontal: 'left',
                vertical: 'top',
              }}
              color="primary"
              overlap="circle"
            >
              <Avatar alt="avatar" src={avatarImage} />
            </StyledBadge>
          </ListItemAvatar>
          <ListItemText
            primary={<Typography>{productReview.title}</Typography>}
            disableTypography
            secondary={
              <>
                <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                  From: {productReview.username}
                </Typography>
                <Typography component="p">{productReview.description}</Typography>
              </>
            }
          />
        </ListItem>
      ))}
    </List>
  );
};

export default ReviewsSection;
