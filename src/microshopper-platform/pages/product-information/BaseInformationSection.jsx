// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';
import monitorImage from 'microshopper-platform/assets/images/monitor.png';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(() => ({
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
}));

type Props = {
  name: string,
  price: number,
  quantity: number,
  manufacturerName: string,
};

const BaseInformationSection = ({ name, price, quantity, manufacturerName }: Props): React.Node => {
  const classes = useStyles();
  return (
    <Grid container spacing={2} direction="row" justify="space-between" alignItems="flex-start">
      <Grid item xs={12} sm={4}>
        <Box component="div">
          <Typography variant="h5" component="h3">
            {name}
          </Typography>
          <Typography component="p">Price: {price}</Typography>
          <Typography component="p">Quantity: {quantity}</Typography>
          <Typography component="p">Manufacturer: {manufacturerName}</Typography>
        </Box>
      </Grid>
      <Grid item xs={12} sm={4}>
        <Box component="div">
          <img src={monitorImage} alt="product_image" className={classes.image} />
        </Box>
      </Grid>
    </Grid>
  );
};

export default BaseInformationSection;
