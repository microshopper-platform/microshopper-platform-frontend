// @flow
import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import type { Product } from 'microshopper-platform/api/generated-sources/product/src';
import phoneImage from 'microshopper-platform/assets/images/phone.png';
import monitorImage from 'microshopper-platform/assets/images/monitor.png';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import MoreIcon from '@material-ui/icons/More';

const useStyles = makeStyles({
  card: {},
  cardContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  informationContent: {},
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 18,
  },
  pos: {
    marginBottom: 12,
  },
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
});

type Props = {
  product: Product,
};

const ProductCard = ({ product }: Props): React.Node => {
  const classes = useStyles();
  const image = React.useMemo<string>(() => (product.category === 'Phones' ? phoneImage : monitorImage), [product]);
  return (
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
        <Box component="div" className={classes.informationContent}>
          <Typography variant="h5" component="h2">
            {product.name}
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            {product.category.name}
          </Typography>
          <Typography variant="body2" component="p">
            Price: {product.price}
          </Typography>
        </Box>
        <Box component="div">
          <img src={image} alt="product_image" className={classes.image} />
        </Box>
      </CardContent>
      <CardActions>
        <Button component={Link} to={`/product/${product.id}`} startIcon={<MoreIcon />}>
          See More
        </Button>
      </CardActions>
    </Card>
  );
};

export default ProductCard;
