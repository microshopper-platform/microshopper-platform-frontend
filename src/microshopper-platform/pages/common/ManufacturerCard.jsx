// @flow
import * as React from 'react';
import type { Manufacturer } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import businessShopImage from 'microshopper-platform/assets/images/business-shop.png';
import MoreIcon from '@material-ui/icons/More';
import { redirectToWithState } from 'microshopper-platform/utils/historyUtils';
import type { SearchProps } from 'microshopper-platform/pages/product-catalog/types.flow';

const useStyles = makeStyles({
  card: {},
  cardContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  informationContent: {},
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 18,
  },
  pos: {
    marginBottom: 12,
  },
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
});

type Props = {
  manufacturer: Manufacturer,
};

const ManufacturerCard = ({ manufacturer }: Props): React.Node => {
  const classes = useStyles();

  const handleSeeTheirProductsButton = React.useCallback(() => {
    const searchProps: SearchProps = {
      manufacturerId: manufacturer.id,
    };

    redirectToWithState({
      pathname: '/product-catalog',
      state: {
        searchProps,
      },
    });
  }, [manufacturer]);

  return (
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
        <Box component="div" className={classes.informationContent}>
          <Typography variant="h5" component="h2">
            {manufacturer.name}
          </Typography>
        </Box>
        <Box component="div">
          <img src={businessShopImage} alt="product_image" className={classes.image} />
        </Box>
      </CardContent>
      <CardActions>
        <Button onClick={handleSeeTheirProductsButton} startIcon={<MoreIcon />}>
          See Their Products
        </Button>
      </CardActions>
    </Card>
  );
};

export default ManufacturerCard;
