// @flow
import * as React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import IconButton from '@material-ui/core/IconButton';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';

type Props = {
  accountLoggedIn: boolean,
  numberOfNotifications: number,
  handleNotificationsDialogState: Function,
  shoppingCartNumberOfItems: number,
  handleLogoutClick: Function,
  handleMobileMenuClose: Function,
  anchorElement: Element,
};

const MobileMenu = ({
  accountLoggedIn,
  numberOfNotifications,
  handleNotificationsDialogState,
  shoppingCartNumberOfItems,
  handleLogoutClick,
  handleMobileMenuClose,
  anchorElement,
}: Props): React.Node => {
  const isMenuOpen = Boolean(anchorElement);
  return (
    <Menu
      anchorEl={anchorElement}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id="mobile-menu"
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMobileMenuClose}
    >
      {accountLoggedIn && shoppingCartNumberOfItems > 0 && (
        <MenuItem onClick={handleMobileMenuClose} component={Link} to="/shopping-cart">
          <IconButton>
            <Badge
              badgeContent={shoppingCartNumberOfItems}
              anchorOrigin={{
                horizontal: 'right',
                vertical: 'top',
              }}
              color="primary"
              overlap="circle"
            >
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
          <p>Shopping Cart</p>
        </MenuItem>
      )}
      {accountLoggedIn && (
        <MenuItem
          onClick={() => {
            handleNotificationsDialogState(true);
            handleMobileMenuClose();
          }}
        >
          <IconButton>
            <Badge
              badgeContent={numberOfNotifications}
              anchorOrigin={{
                horizontal: 'right',
                vertical: 'top',
              }}
              color="primary"
              overlap="circle"
            >
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
      )}
      {accountLoggedIn && (
        <MenuItem onClick={handleMobileMenuClose} component={Link} to="/account-panel">
          <IconButton>
            <AccountCircleIcon />
          </IconButton>
          <p>Account</p>
        </MenuItem>
      )}
      {accountLoggedIn ? (
        <MenuItem
          onClick={() => {
            handleLogoutClick();
            handleMobileMenuClose();
          }}
        >
          <Button color="inherit">Logout</Button>
        </MenuItem>
      ) : (
        <MenuItem onClick={handleMobileMenuClose} component={Link} to="/login">
          <Button>Login</Button>
        </MenuItem>
      )}
    </Menu>
  );
};

export default MobileMenu;
