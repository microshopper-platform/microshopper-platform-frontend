// @flow
import * as React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';

type Props = {
  accountLoggedIn: boolean,
  numberOfNotifications: number,
  handleNotificationsDialogState: Function,
  shoppingCartNumberOfItems: number,
  handleLogoutClick: Function,
};

const DesktopMenu = ({
  accountLoggedIn,
  numberOfNotifications,
  handleNotificationsDialogState,
  shoppingCartNumberOfItems,
  handleLogoutClick,
}: Props): React.Node => (
  <>
    {accountLoggedIn && (
      <>
        {shoppingCartNumberOfItems > 0 && (
          <IconButton component={Link} to="/shopping-cart">
            <Badge
              badgeContent={shoppingCartNumberOfItems}
              anchorOrigin={{
                horizontal: 'right',
                vertical: 'top',
              }}
              color="primary"
              overlap="circle"
            >
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
        )}
        <>
          <IconButton onClick={() => handleNotificationsDialogState(true)}>
            <Badge
              badgeContent={numberOfNotifications}
              anchorOrigin={{
                horizontal: 'right',
                vertical: 'top',
              }}
              color="primary"
              overlap="circle"
            >
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <IconButton component={Link} to="/account-panel">
            <AccountCircleIcon />
          </IconButton>
        </>
      </>
    )}
    <>
      {accountLoggedIn ? (
        <Button color="inherit" onClick={() => handleLogoutClick()}>
          Logout
        </Button>
      ) : (
        <Button component={Link} to="/login" color="inherit">
          Login
        </Button>
      )}
    </>
  </>
);

export default DesktopMenu;
