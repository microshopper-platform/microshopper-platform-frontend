// @flow
import * as React from 'react';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';
import MenuIcon from '@material-ui/icons/Menu';
import { showDrawerAction } from 'microshopper-platform/redux/meta-data/slice';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { getNumberOfItems } from 'microshopper-platform/redux/shopping-cart/selectors';
import type { State } from 'microshopper-platform/redux/types.flow';
import { logoutAccountAction } from 'microshopper-platform/redux/account/slice';
import { isAccountLoggedIn, getAccount } from 'microshopper-platform/redux/account/selectors';
import { getNewApplicationNotifications } from 'microshopper-platform/redux/notification/selectors';
import type { ApplicationNotification } from 'microshopper-platform/redux/notification/types.flow';
import NotificationProvider from 'microshopper-platform/components/common/NotificationProvider';
import type { AbstractAccount } from 'microshopper-platform/redux/account/types.flow';
import NotificationsDialog from 'microshopper-platform/pages/common/notifications-dialog/NotificationsDialog';
import {
  deleteApplicationNotificationAction,
  markApplicationNotificationAsSeenAction,
} from 'microshopper-platform/redux/notification/slice';
import {
  getCurrentPageTitle,
  isConnectedToNetwork,
  isProgressVisible,
} from 'microshopper-platform/redux/meta-data/selectors';
import SignalWifiOffIcon from '@material-ui/icons/SignalWifiOff';
import LinearProgress from '@material-ui/core/LinearProgress';
import DesktopMenu from 'microshopper-platform/pages/common/application-appbar/DesktopMenu';
import MoreIcon from '@material-ui/icons/MoreVert';
import MobileMenu from 'microshopper-platform/pages/common/application-appbar/MobileMenu';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: '20px',
  },
  toolbar: {
    justifyContent: 'space-between',
  },
  leftSide: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  rightSide: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  desktopMenu: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  mobileMenu: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

type Props = {
  showDrawer: Function,
  shoppingCartNumberOfItems: number,
  logoutAccount: Function,
  accountLoggedIn: boolean,
  account: AbstractAccount,
  notifications: Array<ApplicationNotification>,
  markApplicationNotificationAsSeen: Function,
  deleteApplicationNotification: Function,
  currentPageTitle: string,
  connectedToNetwork: boolean,
  progressVisible: boolean,
};

const ApplicationAppBar = ({
  showDrawer,
  shoppingCartNumberOfItems,
  logoutAccount,
  accountLoggedIn,
  account,
  notifications,
  markApplicationNotificationAsSeen,
  deleteApplicationNotification,
  currentPageTitle,
  connectedToNetwork,
  progressVisible,
}: Props): React.Node => {
  const classes = useStyles();

  const [notificationDialogOpen, setNotificationsDialogOpen] = React.useState<boolean>(false);
  const [mobileMenuAnchorElement, setMobileMenuAnchorElement] = React.useState<Element>(null);

  const handleLogoutClick = React.useCallback(() => {
    logoutAccount();
  }, [logoutAccount]);

  const handleMobileMenuClose = React.useCallback(() => {
    setMobileMenuAnchorElement(null);
  }, [setMobileMenuAnchorElement]);

  const handleMobileMenuOpen = React.useCallback(
    (event: Event) => {
      setMobileMenuAnchorElement(event.currentTarget);
    },
    [setMobileMenuAnchorElement],
  );

  return (
    <Box component="div" className={classes.root}>
      <NotificationsDialog
        open={notificationDialogOpen}
        notifications={notifications}
        markApplicationNotificationAsSeen={markApplicationNotificationAsSeen}
        deleteApplicationNotification={deleteApplicationNotification}
        handleDialogState={setNotificationsDialogOpen}
      />
      <AppBar position="fixed">
        <Toolbar className={classes.toolbar}>
          <Box component="div" className={classes.leftSide}>
            <IconButton
              onClick={() => showDrawer()}
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
            <Typography className={classes.title}>{currentPageTitle}</Typography>
          </Box>
          <Box componen="div" className={classes.rightSide}>
            <Box component="div" className={classes.desktopMenu}>
              <DesktopMenu
                accountLoggedIn={accountLoggedIn}
                numberOfNotifications={notifications.length}
                handleNotificationsDialogState={setNotificationsDialogOpen}
                shoppingCartNumberOfItems={shoppingCartNumberOfItems}
                handleLogoutClick={handleLogoutClick}
              />
            </Box>
            {!connectedToNetwork && <SignalWifiOffIcon color="error" />}
            <Box component="div" className={classes.mobileMenu}>
              <IconButton aria-label="show more" aria-haspopup="true" onClick={handleMobileMenuOpen} color="inherit">
                <MoreIcon />
              </IconButton>
            </Box>
          </Box>
        </Toolbar>
        {progressVisible && <LinearProgress color="secondary" />}
      </AppBar>
      {accountLoggedIn && <NotificationProvider account={account} />}
      <MobileMenu
        accountLoggedIn={accountLoggedIn}
        numberOfNotifications={notifications.length}
        handleNotificationsDialogState={setNotificationsDialogOpen}
        shoppingCartNumberOfItems={shoppingCartNumberOfItems}
        handleLogoutClick={handleLogoutClick}
        handleMobileMenuClose={handleMobileMenuClose}
        anchorElement={mobileMenuAnchorElement}
      />
      <Toolbar />
    </Box>
  );
};

const mapStateToProps = (state: State) => ({
  shoppingCartNumberOfItems: getNumberOfItems(state),
  accountLoggedIn: isAccountLoggedIn(state),
  account: getAccount(state),
  notifications: getNewApplicationNotifications(state),
  currentPageTitle: getCurrentPageTitle(state),
  connectedToNetwork: isConnectedToNetwork(state),
  progressVisible: isProgressVisible(state),
});

const mapDispatchToProps = {
  showDrawer: showDrawerAction,
  logoutAccount: logoutAccountAction,
  markApplicationNotificationAsSeen: markApplicationNotificationAsSeenAction,
  deleteApplicationNotification: deleteApplicationNotificationAction,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(ApplicationAppBar);
