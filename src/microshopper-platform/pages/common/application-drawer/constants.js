// @flow

export const linkActiveStyle = {
  fontWeight: 'bold',
  color: 'blue',
};

export const linkStyle = {
  color: 'black',
};
