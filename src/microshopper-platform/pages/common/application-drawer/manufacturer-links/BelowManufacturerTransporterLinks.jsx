// @flow
import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import { NavLink } from 'react-router-dom';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ListItemText from '@material-ui/core/ListItemText';
import MarkunreadMailboxIcon from '@material-ui/icons/MarkunreadMailbox';
import { linkActiveStyle, linkStyle } from 'microshopper-platform/pages/common/application-drawer/constants';

const BelowManufacturerTransportLinks = (): React.Node => (
  <>
    <ListItem component={NavLink} to="/account-panel" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <AccountCircleIcon />
      </ListItemIcon>
      <ListItemText primary="Account Panel" />
    </ListItem>
    <ListItem component={NavLink} to="/orders" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <MarkunreadMailboxIcon />
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItem>
  </>
);

export default BelowManufacturerTransportLinks;
