// @flow
import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import { NavLink } from 'react-router-dom';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import StorefrontIcon from '@material-ui/icons/Storefront';
import ListItemText from '@material-ui/core/ListItemText';
import PostAddIcon from '@material-ui/icons/PostAdd';
import { linkActiveStyle, linkStyle } from 'microshopper-platform/pages/common/application-drawer/constants';
import MarkunreadMailboxIcon from '@material-ui/icons/MarkunreadMailbox';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import VpnKeyIcon from '@material-ui/icons/VpnKey';

const BelowManufacturerAdminLinks = (): React.Node => (
  <>
    <ListItem component={NavLink} to="/account-panel" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <AccountCircleIcon />
      </ListItemIcon>
      <ListItemText primary="Account Panel" />
    </ListItem>
    <ListItem component={NavLink} to="/orders" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <MarkunreadMailboxIcon />
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItem>
    <ListItem component={NavLink} to="/my-products" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <StorefrontIcon />
      </ListItemIcon>
      <ListItemText primary="Your Products" />
    </ListItem>
    <ListItem component={NavLink} to="/add-product" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <PostAddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Product" />
    </ListItem>
    <ListItem component={NavLink} to="/manufacturer-registration-codes" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <VpnKeyIcon />
      </ListItemIcon>
      <ListItemText primary="Registration Codes" />
    </ListItem>
  </>
);

export default BelowManufacturerAdminLinks;
