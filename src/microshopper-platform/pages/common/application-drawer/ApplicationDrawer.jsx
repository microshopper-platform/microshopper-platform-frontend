// @flow
import * as React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import { isDrawerVisible } from 'microshopper-platform/redux/meta-data/selectors';
import { hideDrawerAction } from 'microshopper-platform/redux/meta-data/slice';
import type { State } from 'microshopper-platform/redux/types.flow';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Box from '@material-ui/core/Box';
import type { Role } from 'microshopper-platform/redux/account/types.flow';
import { getAccountRole, isAccountLoggedIn } from 'microshopper-platform/redux/account/selectors';
import AboveLinks from 'microshopper-platform/pages/common/application-drawer/AboveLinks';
import BelowLinks from 'microshopper-platform/pages/common/application-drawer/BelowLinks';

type Props = {
  drawerVisible: boolean,
  hideDrawer: Function,
  accountLoggedIn: boolean,
  accountRole: Role,
};

const ApplicationDrawer = ({ drawerVisible, hideDrawer, accountLoggedIn, accountRole }: Props): React.Node => (
  <Drawer open={drawerVisible} onClose={() => hideDrawer()}>
    <Box component="div" onClick={() => hideDrawer()} onKeyDown={() => hideDrawer()}>
      <AboveLinks accountRole={accountRole} />
      <Divider />
      <BelowLinks accountLoggedIn={accountLoggedIn} accountRole={accountRole} />
    </Box>
  </Drawer>
);

const mapStateToProps = (state: State) => ({
  drawerVisible: isDrawerVisible(state),
  accountLoggedIn: isAccountLoggedIn(state),
  accountRole: getAccountRole(state),
});

const mapDispatchToProps = {
  hideDrawer: hideDrawerAction,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(ApplicationDrawer);
