// @flow
import * as React from 'react';
import { NavLink } from 'react-router-dom';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import { linkActiveStyle, linkStyle } from 'microshopper-platform/pages/common/application-drawer/constants';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MarkunreadMailboxIcon from '@material-ui/icons/MarkunreadMailbox';

const BelowUserLinks = (): React.Node => (
  <>
    <ListItem component={NavLink} to="/account-panel" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <AccountCircleIcon />
      </ListItemIcon>
      <ListItemText primary="Account Panel" />
    </ListItem>
    <ListItem component={NavLink} to="/orders" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <MarkunreadMailboxIcon />
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItem>
    <ListItem component={NavLink} to="/shopping-cart" style={linkStyle} activeStyle={linkActiveStyle}>
      <ListItemIcon>
        <ShoppingCartIcon />
      </ListItemIcon>
      <ListItemText primary="Shopping Cart" />
    </ListItem>
  </>
);

export default BelowUserLinks;
