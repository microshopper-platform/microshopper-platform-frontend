// @flow
import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import { NavLink } from 'react-router-dom';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import HomeIcon from '@material-ui/icons/Home';
import ListItemText from '@material-ui/core/ListItemText';
import StorefrontIcon from '@material-ui/icons/Storefront';
import StoreIcon from '@material-ui/icons/Store';
import List from '@material-ui/core/List';
import type { Role } from 'microshopper-platform/redux/account/types.flow';
import { linkActiveStyle, linkStyle } from 'microshopper-platform/pages/common/application-drawer/constants';
import { isManufacturerRole } from 'microshopper-platform/utils/roleUtils';

type Props = {
  accountRole: Role,
};

const AboveLinks = ({ accountRole }: Props): React.Node => (
  <>
    {!isManufacturerRole(accountRole) && (
      <List>
        <ListItem component={NavLink} to="/" style={linkStyle} activeStyle={linkActiveStyle} exact>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>
        <ListItem component={NavLink} to="/product-catalog" style={linkStyle} activeStyle={linkActiveStyle}>
          <ListItemIcon>
            <StorefrontIcon />
          </ListItemIcon>
          <ListItemText primary="Product Catalog" />
        </ListItem>
        <ListItem component={NavLink} to="/manufacturers" style={linkStyle} activeStyle={linkActiveStyle}>
          <ListItemIcon>
            <StoreIcon />
          </ListItemIcon>
          <ListItemText primary="Manufacturers" />
        </ListItem>
      </List>
    )}
  </>
);

export default AboveLinks;
