// @flow
import * as React from 'react';
import type { Role } from 'microshopper-platform/redux/account/types.flow';
import List from '@material-ui/core/List';
import BelowUserLinks from 'microshopper-platform/pages/common/application-drawer/BelowUserLinks';
import BelowManufacturerAdminLinks from 'microshopper-platform/pages/common/application-drawer/manufacturer-links/BelowManufacturerAdminLinks';
import BelowManufacturerTransportLinks from 'microshopper-platform/pages/common/application-drawer/manufacturer-links/BelowManufacturerTransporterLinks';
import { isManufacturerAdmin, isManufacturerTransporter, isUserRole } from 'microshopper-platform/utils/roleUtils';

type Props = {
  accountLoggedIn: boolean,
  accountRole: Role,
};

const BelowLinks = ({ accountLoggedIn, accountRole }: Props): React.Node => (
  <>
    {accountLoggedIn && (
      <List>
        {isUserRole(accountRole) && <BelowUserLinks />}
        {isManufacturerAdmin(accountRole) && <BelowManufacturerAdminLinks />}
        {isManufacturerTransporter(accountRole) && <BelowManufacturerTransportLinks />}
      </List>
    )}
  </>
);

export default BelowLinks;
