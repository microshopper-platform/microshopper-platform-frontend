// @flow
import * as React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import Box from '@material-ui/core/Box';
import type { ApplicationNotification } from 'microshopper-platform/redux/notification/types.flow';
import NotificationItem from 'microshopper-platform/pages/common/notifications-dialog/NotificationItem';
import List from '@material-ui/core/List';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

type Props = {
  open: boolean,
  notifications: Array<ApplicationNotification>,
  markApplicationNotificationAsSeen: Function,
  deleteApplicationNotification: Function,
  handleDialogState: Function,
};

const Transition = React.forwardRef((props: Object, ref: Object) => <Slide direction="up" ref={ref} {...props} />);

const NotificationsDialog = ({
  open,
  notifications,
  markApplicationNotificationAsSeen,
  deleteApplicationNotification,
  handleDialogState,
}: Props): React.Node => {
  const classes = useStyles();

  return (
    <Box component="div">
      <Dialog fullScreen open={open} onClose={() => handleDialogState(false)} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={() => handleDialogState(false)} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Notifications
            </Typography>
          </Toolbar>
        </AppBar>
        <List>
          {notifications.map(notification => (
            <NotificationItem
              key={notification.id}
              notification={notification}
              markAsSeen={markApplicationNotificationAsSeen}
              deleteNotification={deleteApplicationNotification}
            />
          ))}
        </List>
      </Dialog>
    </Box>
  );
};

export default NotificationsDialog;