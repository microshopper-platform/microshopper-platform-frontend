// @flow
import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import VisibilityIcon from '@material-ui/icons/Visibility';
import CheckIcon from '@material-ui/icons/Check';
import type {
  ApplicationNotification,
  DeleteApplicationNotificationAction,
  MarkApplicationNotificationAsSeenAction,
} from 'microshopper-platform/redux/notification/types.flow';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textItem: {
    flexShrink: 2,
  },
}));

type Props = {
  notification: ApplicationNotification,
  markAsSeen: Function,
  deleteNotification: Function,
};

const NotificationItem = ({ notification, markAsSeen, deleteNotification }: Props): React.Node => {
  const classes = useStyles();
  const handleMarkAsSeenClick = React.useCallback(
    (id: string) => {
      const action: MarkApplicationNotificationAsSeenAction = {
        id,
      };
      markAsSeen(action);
    },
    [markAsSeen],
  );

  const handleDeleteNotificationClick = React.useCallback(
    (id: string) => {
      const action: DeleteApplicationNotificationAction = {
        id,
      };
      deleteNotification(action);
    },
    [deleteNotification],
  );

  return (
    <ListItem className={classes.root}>
      <ListItemText className={classes.textItem} primary={notification.title} secondary={notification.message} />
      <ListItemSecondaryAction>
        {notification.seen ? (
          <IconButton edge="end" aria-label="delete">
            <VisibilityIcon />
          </IconButton>
        ) : (
          <IconButton onClick={() => handleMarkAsSeenClick(notification.id)} edge="end" aria-label="delete">
            <CheckIcon />
          </IconButton>
        )}
        <IconButton onClick={() => handleDeleteNotificationClick(notification.id)} edge="end" aria-label="delete">
          <DeleteForeverIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default NotificationItem;
