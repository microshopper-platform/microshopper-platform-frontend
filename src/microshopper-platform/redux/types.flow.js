// @flow
import type { MetaDataState } from 'microshopper-platform/redux/meta-data/types.flow';
import type { ShoppingCartState } from 'microshopper-platform/redux/shopping-cart/types.flow';
import type { AccountState } from 'microshopper-platform/redux/account/types.flow';

export type State = MetaDataState & ShoppingCartState & AccountState;

export type SagaFunction = Generator<any, any, any>;

export type PayloadAction<P> = {
  payload: P,
  meta?: any,
  error?: any,
}