// @flow
import isEmpty from 'lodash/isEmpty';
import type {
  AccountState,
  AbstractAccount,
  AccountData,
  ManufacturerAccountData,
  Role,
} from 'microshopper-platform/redux/account/types.flow';
import type { State } from 'microshopper-platform/redux/types.flow';

const getAccountState = (state: State): AccountState => state.account;

export const getAccount = (state: State): AbstractAccount => getAccountState(state).account;

export const getAccountData = (state: State): AccountData | ManufacturerAccountData => getAccountState(state).data;

export const isAccountLoggedIn = (state: State): boolean => !isEmpty(getAccountState(state).account);

export const getAccountRole = (state: State): Role => getAccountState(state).account.role;
