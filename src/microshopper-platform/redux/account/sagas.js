// @flow
import type { SagaFunction } from 'microshopper-platform/redux/types.flow';
import { takeEvery, call, put, select } from '@redux-saga/core/effects';
import {
  loginAccountAction,
  storeAccountAction,
  updateAccountDataAction,
  storeUpdatedAccountDataAction,
  emptyAccountAction,
  logoutAccountAction,
} from 'microshopper-platform/redux/account/slice';
import type {
  AbstractAccount,
  StoreAccountAction,
  UpdateAccountDataAction,
  UpdateManufacturerAccountDataAction,
  LoginAccountAction,
  AccountData,
  ManufacturerAccountData,
} from 'microshopper-platform/redux/account/types.flow';
import { getAccountDataByUsername, updateAccount } from 'microshopper-platform/services/account';
import {
  updateManufacturerInformation,
  getManufacturerAccountByUsername,
  updateManufacturerAccount,
} from 'microshopper-platform/services/manufacturer';
import { getAccount } from 'microshopper-platform/redux/account/selectors';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';
import { emptyApplicationNotificationsAction } from 'microshopper-platform/redux/notification/slice';
import { saveTokenToCookies, deleteTokenFromCookies } from 'microshopper-platform/services/auth/utils';
import { resolveSubscriptionForPushNotifications } from 'microshopper-platform/utils/pushNotificationUtils';
import type { TokenData, LoginProps } from 'microshopper-platform/services/auth/types.flow';
import { login } from 'microshopper-platform/services/auth';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';
import { createShowSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/utils';
import { checkForNetworkConnectivitySaga } from 'microshopper-platform/utils/commonSagaUtils';
import { DEFAULT_REDIRECT_TO, MAP_ROLE_WITH_REDIRECT_TO } from 'microshopper-platform/routing/routes';
import { isManufacturerRole, isUserRole } from 'microshopper-platform/utils/roleUtils';
import type { Account } from 'microshopper-platform/api/generated-sources/account/src';
import type { Manufacturer, ManufacturerAccount } from 'microshopper-platform/api/generated-sources/manufacturer/src';

function* loginAccountSaga({ payload }: { payload: LoginAccountAction }): SagaFunction {
  try {
    yield call(checkForNetworkConnectivitySaga);
    const loginProps: LoginProps = payload;
    const tokenData = yield call(login, loginProps);
    yield call(saveTokenToCookies, tokenData);
    yield call(storeAccount, tokenData);
    yield call(resolveSubscriptionForPushNotifications, tokenData.username);
    redirectTo(MAP_ROLE_WITH_REDIRECT_TO.get(tokenData.role));
    const messageAction = createShowSnackbarMessageAction('Login successful.', snackbarMessageType.SUCCESS, false);
    yield put(showSnackbarMessageAction(messageAction));
  } catch (e) {
    const messageAction = createShowSnackbarMessageAction(
      'Error occurred while trying to login.',
      snackbarMessageType.ERROR,
      false,
    );
    console.log(e);
    yield put(showSnackbarMessageAction(messageAction));
  }
}

export function* storeAccount(tokenData: TokenData): SagaFunction {
  try {
    yield call(checkForNetworkConnectivitySaga);
    let data: AccountData | ManufacturerAccountData;
    if (isUserRole(tokenData.role)) {
      data = yield call(getAccountDataByUsername, tokenData.username);
    } else if (isManufacturerRole(tokenData.role)) {
      const manufacturerAccount: ManufacturerAccount = yield call(getManufacturerAccountByUsername, tokenData.username);
      data = {
        id: manufacturerAccount.manufacturer.id,
        name: manufacturerAccount.manufacturer.name,
        description: manufacturerAccount.manufacturer.description,
      };
    }
    const account: AbstractAccount = {
      username: tokenData.username,
      email: tokenData.email,
      role: tokenData.role,
      id: tokenData.id,
      externalId: tokenData.externalId,
    };
    const action: StoreAccountAction = {
      account,
      data,
    };
    yield put(storeAccountAction(action));
  } catch (e) {
    const messageAction = createShowSnackbarMessageAction(
      'Error occurred while trying getting account data.',
      snackbarMessageType.ERROR,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
    console.log(e);
  }
}

function* updateAccountDataSaga({
  payload,
}: {
  payload: UpdateAccountDataAction & UpdateManufacturerAccountDataAction,
}): SagaFunction {
  try {
    yield call(checkForNetworkConnectivitySaga);
    const account = yield select(getAccount);
    if (isUserRole(account.role)) {
      const updateData: Account = {
        username: account.username,
        email: payload.email,
        data: {
          firstName: payload.firstName,
          lastName: payload.lastName,
          country: payload.country,
          city: payload.city,
          address: payload.address,
        },
      };

      yield call(updateAccount, updateData);
      yield put(storeUpdatedAccountDataAction(payload));
    } else if (isManufacturerRole(account.role)) {
      const manufacturerData: Manufacturer = {
        id: payload.id,
        description: payload.description,
      };
      const manufacturerAccountData: ManufacturerAccount = {
        username: account.username,
        email: payload.email,
      };
      yield call(updateManufacturerInformation, manufacturerData);
      yield call(updateManufacturerAccount, manufacturerAccountData);
      yield put(storeUpdatedAccountDataAction(payload));
    }
    const messageAction = createShowSnackbarMessageAction(
      'Successfully updated account.',
      snackbarMessageType.SUCCESS,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  } catch (e) {
    const messageAction = createShowSnackbarMessageAction(
      'Error occurred while trying updating account data.',
      snackbarMessageType.ERROR,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  }
}

function* logoutAccountActionSaga(): SagaFunction {
  try {
    yield put(emptyAccountAction());
    yield put(emptyApplicationNotificationsAction());
    yield call(deleteTokenFromCookies);
    redirectTo(DEFAULT_REDIRECT_TO);
    const messageAction = createShowSnackbarMessageAction('Logout successful.', snackbarMessageType.SUCCESS, false);
    yield put(showSnackbarMessageAction(messageAction));
  } catch (e) {
    console.log('Error while logging out.');
  }
}

export function* loginAccountWatcher(): SagaFunction {
  yield takeEvery(loginAccountAction.toString(), loginAccountSaga);
}

export function* updateAccountDataWatcher(): SagaFunction {
  yield takeEvery(updateAccountDataAction.toString(), updateAccountDataSaga);
}

export function* logoutAccountWatcher(): SagaFunction {
  yield takeEvery(logoutAccountAction.toString(), logoutAccountActionSaga);
}
