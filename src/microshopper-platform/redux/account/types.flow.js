// @flow
import { accountRoles } from 'microshopper-platform/utils/constants';

export type Role = $Keys<typeof accountRoles>;

export type AccountState = {
  account: AbstractAccount,
  data: AccountData | ManufacturerAccountData,
};

export type AbstractAccount = {
  username: string,
  email: string,
  role: Role,
  id: number,
  externalId: number,
};

export type AccountData = {
  firstName: string,
  lastName: string,
  country: string,
  city: string,
  address: string,
};

export type ManufacturerAccountData = {
  id: number,
  name: string,
  description: string,
};

export type StoreAccountAction = {
  account: AbstractAccount,
  data: AccountData | ManufacturerAccountData,
};

export type UpdateAccountDataAction = {
  email: string,
  firstName: string,
  lastName: string,
  country: string,
  city: string,
  address: string,
};

export type UpdateManufacturerAccountDataAction = {
  id: number,
  email: string,
  description: string,
};

export type LoginAccountAction = {
  username: string,
  password: string,
};
