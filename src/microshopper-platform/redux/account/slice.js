// @flow
import pick from 'lodash/pick';
import omit from 'lodash/omit';
import type {
  AccountState,
  UpdateAccountDataAction,
  UpdateManufacturerAccountDataAction,
  StoreAccountAction,
} from 'microshopper-platform/redux/account/types.flow';
import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: AccountState = {
  account: {},
  data: {},
};

export const accountSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    loginAccountAction: () => {},
    storeAccountAction: (state: AccountState, action: PayloadAction<StoreAccountAction>): AccountState => {
      const { payload }: { payload: StoreAccountAction } = action;
      return {
        ...state,
        account: payload.account,
        data: payload.data,
      };
    },
    updateAccountDataAction: () => {},
    storeUpdatedAccountDataAction: (
      state: AccountState,
      action: PayloadAction<UpdateAccountDataAction | UpdateManufacturerAccountDataAction>,
    ): AccountState => {
      const { payload }: { payload: UpdateAccountDataAction | UpdateManufacturerAccountDataAction } = action;
      const { email } = pick(payload, ['email']);
      const data = omit(payload, ['email']);
      return {
        ...state,
        account: { ...state.account, email },
        data: { ...state.data, ...data },
      };
    },
    logoutAccountAction: () => {},
    emptyAccountAction: (state: AccountState): AccountState => ({
      ...state,
      account: {},
      data: {},
    }),
  },
});

export const {
  loginAccountAction,
  storeAccountAction,
  updateAccountDataAction,
  storeUpdatedAccountDataAction,
  logoutAccountAction,
  emptyAccountAction,
} = accountSlice.actions;

export default accountSlice.reducer;
