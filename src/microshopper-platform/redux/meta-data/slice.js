// @flow
import type { MetaDataState } from 'microshopper-platform/redux/meta-data/types.flow';
import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: MetaDataState = {
  drawerVisible: false,
  currentPageTitle: '',
  progressVisible: false,
  connectedToNetwork: true,
};

export const metaDataSlice = createSlice({
  name: 'metaData',
  initialState,
  reducers: {
    showDrawerAction: (state: MetaDataState): MetaDataState => ({
      ...state,
      drawerVisible: true,
    }),
    hideDrawerAction: (state: MetaDataState): MetaDataState => ({
      ...state,
      drawerVisible: false,
    }),
    showProgressAction: (state: MetaDataState): MetaDataState => ({
      ...state,
      progressVisible: true,
    }),
    hideProgressAction: (state: MetaDataState): MetaDataState => ({
      ...state,
      progressVisible: false,
    }),
    setCurrentPageTitleAction: (state: MetaDataState, action: PayloadAction<string>): MetaDataState => ({
      ...state,
      currentPageTitle: action.payload,
    }),
    setNetworkConnectivityStatusAction: (state: MetaDataState, action: PayloadAction<boolean>): MetaDataState => ({
      ...state,
      connectedToNetwork: action.payload,
    }),
  },
});

export const {
  showDrawerAction,
  hideDrawerAction,
  showProgressAction,
  hideProgressAction,
  setCurrentPageTitleAction,
  setNetworkConnectivityStatusAction,
} = metaDataSlice.actions;

export default metaDataSlice.reducer;
