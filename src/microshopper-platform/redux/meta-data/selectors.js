// @flow
import type { State } from 'microshopper-platform/redux/types.flow';
import type { MetaDataState } from 'microshopper-platform/redux/meta-data/types.flow';

const getMetaDataState = (state: State): MetaDataState => state.metaData;

export const isDrawerVisible = (state: State): boolean => getMetaDataState(state).drawerVisible;

export const isProgressVisible = (state: State): boolean => getMetaDataState(state).progressVisible;

export const getCurrentPageTitle = (state: State): string => getMetaDataState(state).currentPageTitle;

export const isConnectedToNetwork = (state: State): string => getMetaDataState(state).connectedToNetwork;
