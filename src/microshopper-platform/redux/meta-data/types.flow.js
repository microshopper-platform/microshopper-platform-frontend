// @flow
export type MetaDataState = {
  drawerVisible: boolean,
  progressVisible: boolean,
  currentPageTitle: string,
  connectedToNetwork: boolean,
};
