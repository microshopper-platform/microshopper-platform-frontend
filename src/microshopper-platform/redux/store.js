// @flow
import { configureStore } from '@reduxjs/toolkit';
import { createBrowserHistory } from 'history';
import createRootReducer from 'microshopper-platform/redux/rootReducer';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'microshopper-platform/redux/rootSaga';
import { offline } from '@redux-offline/redux-offline';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';

export const browserHistory = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const reduxOfflineConfig = {
  ...offlineConfig,
  persistOptions: {
    blacklist: ['metaData', 'account', 'snackbarMessage', 'bootstrap', 'confirmationDialog', 'notifications'],
  },
};

const store = configureStore({
  reducer: createRootReducer(browserHistory),
  middleware: [sagaMiddleware],
  enhancers: [offline({ ...reduxOfflineConfig })],
});

sagaMiddleware.run(rootSaga);

export default store;
