// @flow
import { call, put, takeEvery } from '@redux-saga/core/effects';
import type {
  DeleteApplicationNotificationAction,
  MarkApplicationNotificationAsSeenAction,
} from 'microshopper-platform/redux/notification/types.flow';
import type { SagaFunction } from 'microshopper-platform/redux/types.flow';
import { deleteNotification, markNotificationAsSeen } from 'microshopper-platform/services/notification';
import {
  deleteApplicationNotificationAction,
  deleteApplicationNotification,
  markApplicationNotificationAsSeenAction,
  markApplicationNotificationAsSeen,
} from 'microshopper-platform/redux/notification/slice';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';
import { createShowSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/utils';

function* deleteApplicationNotificationSaga({
  payload,
}: {
  payload: DeleteApplicationNotificationAction,
}): SagaFunction {
  try {
    yield call(deleteNotification, payload.id);
    yield put(deleteApplicationNotification(payload));
    const messageAction = createShowSnackbarMessageAction(
      'Successfully deleted notification',
      snackbarMessageType.SUCCESS,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  } catch (e) {
    const messageAction = createShowSnackbarMessageAction(
      'Error occurred while trying deleting notification',
      snackbarMessageType.ERROR,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  }
}

function* markApplicationNotificationAsSeenSaga({
  payload,
}: {
  payload: MarkApplicationNotificationAsSeenAction,
}): SagaFunction {
  try {
    yield call(markNotificationAsSeen, payload.id);
    yield put(markApplicationNotificationAsSeen(payload));
    const messageAction = createShowSnackbarMessageAction(
      'Successfully marked notification as seen',
      snackbarMessageType.SUCCESS,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  } catch (e) {
    const messageAction = createShowSnackbarMessageAction(
      'Error occurred while trying marking notification as seen.',
      snackbarMessageType.ERROR,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  }
}

export function* deleteApplicationNotificationWatcher(): SagaFunction {
  yield takeEvery(deleteApplicationNotificationAction.toString(), deleteApplicationNotificationSaga);
}

export function* markApplicationNotificationAsSeenWatcher(): SagaFunction {
  yield takeEvery(markApplicationNotificationAsSeenAction.toString(), markApplicationNotificationAsSeenSaga);
}
