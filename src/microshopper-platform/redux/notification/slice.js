// @flow
import { createSlice } from '@reduxjs/toolkit';
import type {
  ApplicationNotificationsState,
  AddApplicationNotificationAction,
  DeleteApplicationNotificationAction,
  MarkApplicationNotificationAsSeenAction,
} from 'microshopper-platform/redux/notification/types.flow';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: ApplicationNotificationsState = {
  notifications: [],
};

export const applicationNotificationsSlice = createSlice({
  name: 'applicationNotifications',
  initialState,
  reducers: {
    addApplicationNotificationAction: (
      state: ApplicationNotificationsState,
      action: PayloadAction<AddApplicationNotificationAction>,
    ): ApplicationNotificationsState => {
      const hasNotification = state.notifications.some(notification => notification.id === action.payload.id);
      if (!hasNotification) {
        return {
          ...state,
          notifications: [...state.notifications, action.payload],
        };
      }
      return {
        ...state,
        notifications: [...state.notifications],
      };
    },
    deleteApplicationNotificationAction: () => {},
    deleteApplicationNotification: (
      state: ApplicationNotificationsState,
      action: PayloadAction<DeleteApplicationNotificationAction>,
    ): ApplicationNotificationsState => ({
      ...state,
      notifications: state.notifications.filter(notification => notification.id !== action.payload.id),
    }),
    markApplicationNotificationAsSeenAction: () => {},
    markApplicationNotificationAsSeen: (
      state: ApplicationNotificationsState,
      action: PayloadAction<MarkApplicationNotificationAsSeenAction>,
    ): ApplicationNotificationsState => ({
      ...state,
      notifications: state.notifications.map(notification =>
        notification.id === action.payload.id ? { ...notification, seen: true } : notification,
      ),
    }),
    emptyApplicationNotificationsAction: (state: ApplicationNotificationsState): ApplicationNotificationsState => ({
      ...state,
      notifications: [],
    }),
  },
});

export const {
  addApplicationNotificationAction,
  deleteApplicationNotificationAction,
  deleteApplicationNotification,
  markApplicationNotificationAsSeenAction,
  markApplicationNotificationAsSeen,
  emptyApplicationNotificationsAction,
} = applicationNotificationsSlice.actions;

export default applicationNotificationsSlice.reducer;
