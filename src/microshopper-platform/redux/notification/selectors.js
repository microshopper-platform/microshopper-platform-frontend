// @flow
import type { State } from 'microshopper-platform/redux/types.flow';
import type {
  ApplicationNotification,
  ApplicationNotificationsState,
} from 'microshopper-platform/redux/notification/types.flow';

const getApplicationNotificationsState = (state: State): ApplicationNotificationsState => state.notifications;

export const getApplicationNotifications = (state: State): Array<ApplicationNotification> =>
  getApplicationNotificationsState(state).notifications;

export const getNewApplicationNotifications = (state: State): Array<ApplicationNotification> =>
  getApplicationNotificationsState(state).notifications.filter(notification => notification.seen === false);
