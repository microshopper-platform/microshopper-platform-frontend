// @flow

export type ApplicationNotificationsState = {
  notifications: Array<ApplicationNotification>,
};

export type ApplicationNotification = {
  id: string,
  title: string,
  message: string,
  username: string,
  seen: boolean,
};

export type AddApplicationNotificationAction = ApplicationNotification;

export type MarkApplicationNotificationAsSeenAction = {
  id: string,
};

export type DeleteApplicationNotificationAction = {
  id: string,
};
