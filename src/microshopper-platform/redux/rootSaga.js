// @flow
import { all } from 'redux-saga/effects';
import type { SagaFunction } from 'microshopper-platform/redux/types.flow';
import {
  loginAccountWatcher,
  updateAccountDataWatcher,
  logoutAccountWatcher,
} from 'microshopper-platform/redux/account/sagas';
import {
  deleteApplicationNotificationWatcher,
  markApplicationNotificationAsSeenWatcher,
} from 'microshopper-platform/redux/notification/sagas';
import { initializeBootstrapWatcher } from 'microshopper-platform/redux/bootstrap/sagas';
import { checkoutCartSagaWatcher } from 'microshopper-platform/redux/shopping-cart/sagas';

export default function* rootSaga(): SagaFunction {
  yield all([
    loginAccountWatcher(),
    updateAccountDataWatcher(),
    deleteApplicationNotificationWatcher(),
    markApplicationNotificationAsSeenWatcher(),
    initializeBootstrapWatcher(),
    logoutAccountWatcher(),
    checkoutCartSagaWatcher(),
  ]);
}
