// @flow
import { put, takeEvery, call, select } from '@redux-saga/core/effects';
import type { SagaFunction } from 'microshopper-platform/redux/types.flow';
import { getShoppingCart } from 'microshopper-platform/redux/shopping-cart/selectors';
import type { CheckoutCartAction, ShoppingCartItem } from 'microshopper-platform/redux/shopping-cart/types.flow';
import type { AbstractAccount } from 'microshopper-platform/redux/account/types.flow';
import { getAccount } from 'microshopper-platform/redux/account/selectors';
import type { NewOrder, Product } from 'microshopper-platform/api/generated-sources/order/src';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';
import { addNewOrder } from 'microshopper-platform/services/order';
import { checkoutCartAction, emptyCartAction } from 'microshopper-platform/redux/shopping-cart/slice';
import { redirectTo } from 'microshopper-platform/utils/historyUtils';
import { createShowSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/utils';
import { checkForNetworkConnectivitySaga } from 'microshopper-platform/utils/commonSagaUtils';

function* checkoutCartSaga({ payload }: { payload: CheckoutCartAction }): SagaFunction {
  try {
    yield call(checkForNetworkConnectivitySaga);
    console.log('Dummy form values:', payload);
    const shoppingCart: Array<ShoppingCartItem> = yield select(getShoppingCart);
    const account: AbstractAccount = yield select(getAccount);
    const newOrder: NewOrder = {
      username: account.username,
      products: shoppingCart.map((item: ShoppingCartItem): Product => ({
        productId: item.productId,
        manufacturerId: item.manufacturerId,
        price: item.productPrice,
        quantity: item.productQuantity,
      })),
    };
    yield call(addNewOrder, newOrder);
    yield put(emptyCartAction());
    redirectTo('/');
    const messageAction = createShowSnackbarMessageAction(
      'Your purchase is successful.',
      snackbarMessageType.SUCCESS,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  } catch (e) {
    const messageAction = createShowSnackbarMessageAction(
      'Error occurred while processing your purchase.',
      snackbarMessageType.ERROR,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
  }
}

export function* checkoutCartSagaWatcher(): SagaFunction {
  yield takeEvery(checkoutCartAction.toString(), checkoutCartSaga);
}
