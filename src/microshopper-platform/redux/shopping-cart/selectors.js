// @flow
import type { State } from 'microshopper-platform/redux/types.flow';
import type { ShoppingCartState, ShoppingCartItem } from 'microshopper-platform/redux/shopping-cart/types.flow';
import find from 'lodash/find';

const getShoppingCartState = (state: State): ShoppingCartState => state.shoppingCart;

export const getShoppingCart = (state: State): Array<ShoppingCartItem> => getShoppingCartState(state).items;

export const getNumberOfItems = (state: State): number => getShoppingCart(state).length;

export const getItem = (state: State, productId: number): ShoppingCartItem =>
  find(getShoppingCart(state), item => item.productId === productId);

export const getItemQuantity = (state: State, productId: number): number => {
  const item = getItem(state, productId);
  return item ? item.productQuantity : 0;
};

export const isProductInCart = (state: State, productId: number): boolean =>
  getShoppingCart(state).some(item => item.productId === productId);
