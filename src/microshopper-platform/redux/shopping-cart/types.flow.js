// @flow

export type ShoppingCartState = {
  items: Array<ShoppingCartItem>,
};

export type ShoppingCartItem = {
  productId: number,
  manufacturerId: number,
  productName: string,
  productPrice: number,
  productQuantity: number,
};

export type AddItemAction = ShoppingCartItem;

export type UpdateItemAction = {
  productId: number,
  productQuantity: number,
};

export type DeleteItemAction = {
  productId: number,
};

export type CheckoutCartAction = {
  firstName: string,
  lastName: string,
  country: string,
  city: string,
  billingAddress: string,
  creditCardNumber: string,
  creditCardSecurityNumber: string,
};
