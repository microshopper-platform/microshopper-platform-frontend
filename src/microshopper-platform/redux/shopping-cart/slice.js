// @flow
import { createSlice } from '@reduxjs/toolkit';
import type {
  ShoppingCartState,
  AddItemAction,
  DeleteItemAction,
  UpdateItemAction,
} from 'microshopper-platform/redux/shopping-cart/types.flow';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: ShoppingCartState = {
  items: [],
};

export const shoppingCartSlice = createSlice({
  name: 'shoppingCart',
  initialState,
  reducers: {
    addItemAction: (state: ShoppingCartState, action: PayloadAction<AddItemAction>): ShoppingCartState => ({
      ...state,
      items: [...state.items, action.payload],
    }),
    deleteItemAction: (state: ShoppingCartState, action: PayloadAction<DeleteItemAction>): ShoppingCartState => ({
      ...state,
      items: state.items.filter(item => item.productId !== action.payload.productId),
    }),
    updateItemAction: (state: ShoppingCartState, action: PayloadAction<UpdateItemAction>): ShoppingCartState => ({
      ...state,
      items: state.items.map(item =>
        item.productId === action.payload.productId && action.payload.productQuantity !== 0
          ? { ...item, productQuantity: action.payload.productQuantity }
          : item,
      ),
    }),
    emptyCartAction: (state: ShoppingCartState): ShoppingCartState => ({
      ...state,
      items: [],
    }),
    checkoutCartAction: () => {},
  },
});

export const {
  addItemAction,
  deleteItemAction,
  updateItemAction,
  emptyCartAction,
  checkoutCartAction,
} = shoppingCartSlice.actions;

export default shoppingCartSlice.reducer;
