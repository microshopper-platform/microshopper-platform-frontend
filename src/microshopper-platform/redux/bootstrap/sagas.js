// @flow
import { put, takeEvery, call } from '@redux-saga/core/effects';
import isEmpty from 'lodash/isEmpty';
import type { TokenData } from 'microshopper-platform/services/auth/types.flow';
import { getTokenFromCookies } from 'microshopper-platform/services/auth/utils';
import { bootstrapStatus } from 'microshopper-platform/redux/bootstrap/types.flow';
import { initializeBootstrapAction, setBootstrapStatusAction } from 'microshopper-platform/redux/bootstrap/slice';
import type { SagaFunction } from 'microshopper-platform/redux/types.flow';
import { storeAccount } from 'microshopper-platform/redux/account/sagas';
import { resolveSubscriptionForPushNotifications } from 'microshopper-platform/utils/pushNotificationUtils';

function* initializeBootstrapSaga(): SagaFunction {
  try {
    const tokenData: TokenData = yield call(getTokenFromCookies);
    if (!isEmpty(tokenData)) {
      yield call(storeAccount, tokenData);
      yield call(resolveSubscriptionForPushNotifications, tokenData.username);
    }
    yield put(setBootstrapStatusAction(bootstrapStatus.BOOTSTRAP_SUCCEEDED));
  } catch (e) {
    console.log('Error while bootstrapping application');
  }
}

export function* initializeBootstrapWatcher(): SagaFunction {
  yield takeEvery(initializeBootstrapAction.toString(), initializeBootstrapSaga);
}
