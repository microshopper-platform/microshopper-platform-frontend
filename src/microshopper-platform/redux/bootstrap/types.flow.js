// @flow

export const bootstrapStatus = {
  BOOTSTRAP_SUCCEEDED: 'BOOTSTRAP_SUCCEEDED',
  BOOTSTRAP_FAILED: 'BOOTSTRAP_FAILED',
  BOOTSTRAP_NOT_STARTED: 'BOOTSTRAP_NOT_STARTED',
};

export type BootstrapStatus = $Keys<typeof bootstrapStatus>;

export type BootstrapState = {
  status: BootstrapStatus,
};

export type SetBootstrapStatusAction = {
  status: BootstrapStatus,
};
