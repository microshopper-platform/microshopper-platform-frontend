// @flow
import type { BootstrapState } from 'microshopper-platform/redux/bootstrap/types.flow';
import { createSlice } from '@reduxjs/toolkit';
import { bootstrapStatus } from './types.flow';
import type { SetBootstrapStatusAction } from 'microshopper-platform/redux/bootstrap/types.flow';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: BootstrapState = {
  status: bootstrapStatus.BOOTSTRAP_NOT_STARTED,
};

export const bootstrapSlice = createSlice({
  name: 'bootstrap',
  initialState,
  reducers: {
    initializeBootstrapAction: () => {},
    setBootstrapStatusAction: (
      state: BootstrapState,
      action: PayloadAction<SetBootstrapStatusAction>,
    ): BootstrapState => ({
      ...state,
      status: action.payload,
    }),
  },
});

export const { initializeBootstrapAction, setBootstrapStatusAction } = bootstrapSlice.actions;

export default bootstrapSlice.reducer;
