// @flow
import type { State } from 'microshopper-platform/redux/types.flow';
import type { BootstrapState, BootstrapStatus } from 'microshopper-platform/redux/bootstrap/types.flow';

const getBootstrapSlice = (state: State): BootstrapState => state.bootstrap;

export const getBootstrapStatus = (state: State): BootstrapStatus => getBootstrapSlice(state).status;
