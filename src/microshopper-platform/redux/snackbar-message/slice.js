// @flow
import { createSlice } from '@reduxjs/toolkit';
import type {
  ShowSnackbarMessageAction,
  SnackbarMessageState,
} from 'microshopper-platform/redux/snackbar-message/types.flow';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: SnackbarMessageState = {
  snackbarMessage: {},
};

export const snackbarMessageSlice = createSlice({
  name: 'snackbarMessage',
  initialState,
  reducers: {
    showSnackbarMessageAction: (
      state: SnackbarMessageState,
      action: PayloadAction<ShowSnackbarMessageAction>,
    ): SnackbarMessageState => ({
      ...state,
      snackbarMessage: action.payload,
    }),
    closeSnackbarMessageAction: (state: SnackbarMessageState): SnackbarMessageState => ({
      ...state,
      snackbarMessage: {},
    }),
  },
});

export const { showSnackbarMessageAction, closeSnackbarMessageAction } = snackbarMessageSlice.actions;

export default snackbarMessageSlice.reducer;
