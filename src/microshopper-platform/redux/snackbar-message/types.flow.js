// @flow
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';

export type SnackbarMessageType = $Keys<typeof snackbarMessageType>;

export type SnackbarMessageState = {
  snackbarMessage: SnackbarMessage,
};

export type SnackbarMessage = {
  message: string,
  type: SnackbarMessageType,
  onOverlay: boolean,
};

export type ShowSnackbarMessageAction = SnackbarMessage;
