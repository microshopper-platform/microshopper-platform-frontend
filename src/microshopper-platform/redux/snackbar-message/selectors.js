// @flow
import isEmpty from 'lodash/isEmpty';
import type { State } from 'microshopper-platform/redux/types.flow';
import type { SnackbarMessage, SnackbarMessageState } from 'microshopper-platform/redux/snackbar-message/types.flow';

const getSnackbarMessageState = (state: State): SnackbarMessageState => state.snackbarMessage;

export const getSnackbarMessage = (state: State): SnackbarMessage => getSnackbarMessageState(state).snackbarMessage;

export const isSnackbarMessagePresent = (state: State): boolean => !isEmpty(getSnackbarMessage(state));
