// @flow
import type {
  ShowSnackbarMessageAction,
  SnackbarMessageType,
} from 'microshopper-platform/redux/snackbar-message/types.flow';

export const createShowSnackbarMessageAction = (
  message: string,
  type: SnackbarMessageType,
  onOverlay: boolean,
): ShowSnackbarMessageAction => ({
  message,
  type,
  onOverlay,
});
