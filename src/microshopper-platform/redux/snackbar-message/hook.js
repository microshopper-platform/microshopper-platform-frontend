// @flow
import { useDispatch } from 'react-redux';
import { createShowSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/utils';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';
import type { SnackbarMessageType } from 'microshopper-platform/redux/snackbar-message/types.flow';

export interface SnackbarMessageInterface {
  snowSnackbarMessage(message: string, type: SnackbarMessageType, onOverlay: false): void;
}

export const useSnackbarMessage = (): SnackbarMessageInterface => {
  const dispatch = useDispatch();
  return {
    showSnackbarMessage(message: string, type: SnackbarMessageType, onOverlay: false) {
      dispatch(showSnackbarMessageAction(createShowSnackbarMessageAction(message, type, onOverlay)));
    },
  };
};
