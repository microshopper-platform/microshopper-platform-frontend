// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import metaDataReducer from 'microshopper-platform/redux/meta-data/slice';
import shoppingCartReducer from 'microshopper-platform/redux/shopping-cart/slice';
import accountReducer from 'microshopper-platform/redux/account/slice';
import applicationNotificationsReducer from 'microshopper-platform/redux/notification/slice';
import snackbarMessageReducer from 'microshopper-platform/redux/snackbar-message/slice';
import bootstrapReducer from 'microshopper-platform/redux/bootstrap/slice';
import confirmationDialogReducer from 'microshopper-platform/redux/confirmation-dialog/slice';

const createRootReducer = (browserHistory: History) =>
  combineReducers({
    router: connectRouter(browserHistory),
    metaData: metaDataReducer,
    shoppingCart: shoppingCartReducer,
    account: accountReducer,
    notifications: applicationNotificationsReducer,
    snackbarMessage: snackbarMessageReducer,
    bootstrap: bootstrapReducer,
    confirmationDialog: confirmationDialogReducer,
  });

export default createRootReducer;
