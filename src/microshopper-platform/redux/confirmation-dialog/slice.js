// @flow
import { createSlice } from '@reduxjs/toolkit';
import type {
  ConfirmationDialogState,
  ShowConfirmationDialogAction,
} from 'microshopper-platform/redux/confirmation-dialog/types.flow';
import type { PayloadAction } from 'microshopper-platform/redux/types.flow';

const initialState: ConfirmationDialogState = {
  confirmationDialog: {},
};

export const confirmationDialogSlice = createSlice({
  name: 'confirmationDialog',
  initialState,
  reducers: {
    showConfirmationDialogAction: (
      state: ConfirmationDialogState,
      action: PayloadAction<ShowConfirmationDialogAction>,
    ): ConfirmationDialogState => ({
      ...state,
      confirmationDialog: action.payload,
    }),
    closeConfirmationDialogAction: (state: ConfirmationDialogState): ConfirmationDialogState => ({
      ...state,
      confirmationDialog: {},
    }),
  },
});

export const { showConfirmationDialogAction, closeConfirmationDialogAction } = confirmationDialogSlice.actions;

export default confirmationDialogSlice.reducer;
