// @flow
import isEmpty from 'lodash/isEmpty';
import type {
  ConfirmationDialog,
  ConfirmationDialogState,
} from 'microshopper-platform/redux/confirmation-dialog/types.flow';
import type { State } from 'microshopper-platform/redux/types.flow';

const getConfirmationDialogState = (state: State): ConfirmationDialogState => state.confirmationDialog;

export const getConfirmationDialog = (state: State): ConfirmationDialog =>
  getConfirmationDialogState(state).confirmationDialog;

export const isConfirmationDialogVisible = (state: State): boolean => !isEmpty(getConfirmationDialog(state));
