// @flow

export type ConfirmationDialogState = {
  confirmationDialog: ConfirmationDialog,
};

export type ConfirmationDialog = {
  callback: Function,
  title: string,
  message: string,
};

export type ShowConfirmationDialogAction = ConfirmationDialog;
