// @flow
import * as React from 'react';
import { accountRoles } from 'microshopper-platform/utils/constants';

export type ApplicationRoute = {
  path: string,
  component: React.Component<*>,
  title: string,
  exact: boolean,
  authorizedRoles: Array<$Keys<typeof accountRoles>>,
  restrictedRoles: Array<$Keys<typeof accountRoles>>,
  redirectTo?: string,
  withAuthorization: boolean,
};
