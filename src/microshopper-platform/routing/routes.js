// @flow
import Home from 'microshopper-platform/pages/home/Home';
import ProductInformation from 'microshopper-platform/pages/product-information/ProductInformation';
import Manufacturers from 'microshopper-platform/pages/manufacturers/Manufacturers';
import LoginPage from 'microshopper-platform/pages/login-register/LoginPage';
import RegisterPage from 'microshopper-platform/pages/login-register/RegisterPage';
import ProductCatalog from 'microshopper-platform/pages/product-catalog/ProductCatalog';
import ShoppingCart from 'microshopper-platform/pages/shopping-cart/ShoppingCart';
import { accountRoles } from 'microshopper-platform/utils/constants';
import AddProductReview from 'microshopper-platform/pages/add-product-review/AddProductReview';
import AccountPanel from 'microshopper-platform/pages/account-panel/AccountPanel';
import AddEditProduct from 'microshopper-platform/pages/add-edit-product/AddEditProduct';
import ManufacturerProducts from 'microshopper-platform/pages/manufacturer-products/ManufacturerProducts';
import type { ApplicationRoute } from 'microshopper-platform/routing/types.flow';
import CheckoutPage from 'microshopper-platform/pages/checkout/CheckoutPage';
import OrdersPage from 'microshopper-platform/pages/orders/OrdersPage';
import OrderDetails from 'microshopper-platform/pages/orders/OrderDetails';
import ManufacturerRegistrationCodes from 'microshopper-platform/pages/manufacturer-registration-codes/ManufacturerRegistrationCodes';

const ALL_ROLES = [
  accountRoles.ROLE_MANUFACTURER_ADMIN,
  accountRoles.ROLE_MANUFACTURER_TRANSPORTER,
  accountRoles.ROLE_USER,
];

const MANUFACTURER_ROLES = [accountRoles.ROLE_MANUFACTURER_ADMIN, accountRoles.ROLE_MANUFACTURER_TRANSPORTER];

export const DEFAULT_REDIRECT_TO = '/';

export const MAP_ROLE_WITH_REDIRECT_TO = new Map<$Keys<typeof accountRoles>, string>([
  [accountRoles.ROLE_USER, '/'],
  [accountRoles.ROLE_MANUFACTURER_ADMIN, '/my-products'],
  [accountRoles.ROLE_MANUFACTURER_TRANSPORTER, '/orders'],
]);

const HOME_ROUTE: ApplicationRoute = {
  path: '/',
  component: Home,
  title: 'Home',
  exact: true,
  restrictedRoles: MANUFACTURER_ROLES,
  withAuthorization: false,
};

const PRODUCT_INFORMATION_ROUTE: ApplicationRoute = {
  path: '/product/:productId',
  component: ProductInformation,
  title: 'Product Information',
  exact: false,
  restrictedRoles: MANUFACTURER_ROLES,
  withAuthorization: false,
};

const MANUFACTURERS_ROUTE: ApplicationRoute = {
  path: '/manufacturers',
  component: Manufacturers,
  title: 'Manufacturers',
  exact: true,
  restrictedRoles: MANUFACTURER_ROLES,
  withAuthorization: false,
};

const LOGIN_ROUTE: ApplicationRoute = {
  path: '/login',
  component: LoginPage,
  title: 'Login',
  exact: true,
  restrictedRoles: [],
  withAuthorization: false,
};

const REGISTER_ROUTE: ApplicationRoute = {
  path: '/register',
  component: RegisterPage,
  title: 'Register',
  exact: true,
  restrictedRoles: [],
  withAuthorization: false,
};

const PRODUCT_CATALOG_ROUTE: ApplicationRoute = {
  path: '/product-catalog',
  component: ProductCatalog,
  title: 'Product Catalog',
  exact: true,
  restrictedRoles: MANUFACTURER_ROLES,
  withAuthorization: false,
};

const SHOPPING_CART_ROUTE: ApplicationRoute = {
  path: '/shopping-cart',
  component: ShoppingCart,
  title: 'Shopping Cart',
  exact: true,
  authorizedRoles: [accountRoles.ROLE_USER],
  redirectTo: '/login',
  restrictedRoles: [],
  withAuthorization: true,
};

const ADD_PRODUCT_REVIEW_ROUTE: ApplicationRoute = {
  path: '/add-product-review',
  component: AddProductReview,
  title: 'Add Product Review',
  exact: true,
  authorizedRoles: [accountRoles.ROLE_USER],
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};
const ACCOUNT_PANEL_ROUTE: ApplicationRoute = {
  path: '/account-panel',
  component: AccountPanel,
  title: 'Account Panel',
  exact: true,
  authorizedRoles: ALL_ROLES,
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};
const ADD_EDIT_PRODUCT: ApplicationRoute = {
  path: '/add-product',
  component: AddEditProduct,
  title: 'Add Product',
  exact: true,
  authorizedRoles: [accountRoles.ROLE_MANUFACTURER_ADMIN],
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};

const MANUFACTURER_PRODUCTS_ROUTE: ApplicationRoute = {
  path: '/my-products',
  component: ManufacturerProducts,
  title: 'Manufacturer Products',
  exact: true,
  authorizedRoles: [accountRoles.ROLE_MANUFACTURER_ADMIN],
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};

const CHECKOUT_ROUTE: ApplicationRoute = {
  path: '/checkout',
  component: CheckoutPage,
  title: 'Checkout',
  exact: true,
  authorizedRoles: [accountRoles.ROLE_USER],
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};

const ORDERS_ROUTE: ApplicationRoute = {
  path: '/orders',
  component: OrdersPage,
  title: 'Orders',
  exact: true,
  authorizedRoles: ALL_ROLES,
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};

const ORDER_DETAILS_ROUTE: ApplicationRoute = {
  path: '/order/:orderId',
  component: OrderDetails,
  title: 'Order Details',
  exact: false,
  authorizedRoles: ALL_ROLES,
  redirectTo: '/login',
  withAuthorization: true,
  restrictedRoles: [],
};

const MANUFACTURER_REGISTRATION_CODES_ROUTE: ApplicationRoute = {
  path: '/manufacturer-registration-codes',
  component: ManufacturerRegistrationCodes,
  title: 'Manufacturer Registration Codes',
  exact: true,
  authorizedRoles: [accountRoles.ROLE_MANUFACTURER_ADMIN],
  withAuthorization: true,
  restrictedRoles: [],
};

export const ROUTES: Array<ApplicationRoute> = [
  HOME_ROUTE,
  PRODUCT_INFORMATION_ROUTE,
  MANUFACTURERS_ROUTE,
  LOGIN_ROUTE,
  REGISTER_ROUTE,
  PRODUCT_CATALOG_ROUTE,
];

export const AUTHORIZED_ROUTES: Array<ApplicationRoute> = [
  SHOPPING_CART_ROUTE,
  ADD_PRODUCT_REVIEW_ROUTE,
  ACCOUNT_PANEL_ROUTE,
  ADD_EDIT_PRODUCT,
  MANUFACTURER_PRODUCTS_ROUTE,
  CHECKOUT_ROUTE,
  ORDERS_ROUTE,
  ORDER_DETAILS_ROUTE,
  MANUFACTURER_REGISTRATION_CODES_ROUTE,
];

export const ALL_ROUTES: Array<ApplicationRoute> = [...ROUTES, ...AUTHORIZED_ROUTES];
