// @flow
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { createShowSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/utils';

export const isConnectedToNetwork = (): boolean => navigator.onLine;

export const onNetworkConnectivityChangesListeners = (onOnline: Function, onOffline: Function) => {
  window.addEventListener('offline', onOffline);
  window.addEventListener('online', onOnline);
};

export const removeOnNetworkConnectivityChangesListeners = (onOnline: Function, onOffline: Function) => {
  window.removeEventListener('offline', onOffline);
  window.removeEventListener('online', onOnline);
};

export const onOnlineSnackbarMessage = createShowSnackbarMessageAction(
  'Network connectivity established.',
  snackbarMessageType.SUCCESS,
  false,
);

export const onOfflineSnackbarMessage = createShowSnackbarMessageAction(
  'Lost network connectivity.',
  snackbarMessageType.ERROR,
  false,
);
