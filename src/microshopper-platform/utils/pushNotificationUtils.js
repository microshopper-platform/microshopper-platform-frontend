// @flow
import { pushNotificationsPublicApiKey } from 'microshopper-platform/utils/constants';
import { subscribeForPushNotifications } from 'microshopper-platform/services/notification';

export const urlB64ToUint8Array = (base64String: string): Array<number> => {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; i += 1) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
};

const checkSupportForPushNotification = (): boolean => 'serviceWorker' in navigator && 'PushManager' in window;

const isNotificationPermissionGranted = (): boolean => Notification.permission === 'granted';

const isNotificationPermissionBlocked = (): boolean => Notification.permission === 'blocked';

export const resolveSubscriptionForPushNotifications = (username: string) => {
  if (checkSupportForPushNotification()) {
    if (isNotificationPermissionGranted()) {
      resolveSubscriptionForPushNotificationsCallback(username);
    } else if (isNotificationPermissionBlocked()) {
      console.log('Notifications permission is blocked');
    } else {
      Notification.requestPermission().then((permission: string) => {
        if (permission === 'granted') {
          resolveSubscriptionForPushNotificationsCallback(username);
        }
      });
    }
  }
};

const resolveSubscriptionForPushNotificationsCallback = (username: string) => {
  navigator.serviceWorker.getRegistration().then((registration: Object) => {
    registration.pushManager
      .getSubscription()
      .then((subscription: Object) => {
        if (!subscription) {
          subscribeForPushNotificationsServiceWorker(username);
          return;
        }
        sendSubscriptionToNotificationService(subscription, username);
      })
      .catch(() => {
        console.log('Error occurred while resolving subscription for push notifications.');
      });
  });
};

const subscribeForPushNotificationsServiceWorker = (username: string) => {
  navigator.serviceWorker.getRegistration().then((registration: Object) => {
    registration.pushManager
      .subscribe({
        userVisibleOnly: true,
        applicationServerKey: urlB64ToUint8Array(pushNotificationsPublicApiKey),
      })
      .then((subscription: Object) => {
        sendSubscriptionToNotificationService(subscription, username);
      })
      .catch(() => {
        console.log('Did not subscribe.');
      });
  });
};

const sendSubscriptionToNotificationService = (subscription: Object, username: string) => {
  subscribeForPushNotifications(username, subscription.toJSON());
};
