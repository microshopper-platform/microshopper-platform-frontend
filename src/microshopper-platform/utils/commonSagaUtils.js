// @flow
import type { SagaFunction } from 'microshopper-platform/redux/types.flow';
import { put, select } from '@redux-saga/core/effects';
import { isConnectedToNetwork } from 'microshopper-platform/redux/meta-data/selectors';
import { createShowSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/utils';
import { snackbarMessageType } from 'microshopper-platform/components/common/SnackbarMessage';
import { showSnackbarMessageAction } from 'microshopper-platform/redux/snackbar-message/slice';

export function* checkForNetworkConnectivitySaga(): SagaFunction {
  const connectedToNetwork = yield select(isConnectedToNetwork);
  if (!connectedToNetwork) {
    const messageAction = createShowSnackbarMessageAction(
      'Not connected to network.',
      snackbarMessageType.ERROR,
      false,
    );
    yield put(showSnackbarMessageAction(messageAction));
    throw new Error('Not connected to network');
  }
}
