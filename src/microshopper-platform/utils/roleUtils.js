// @flow

import { manufacturerRoles, accountRoles } from 'microshopper-platform/utils/constants';

export const isManufacturerRole = (role: $Keys<typeof accountRoles>): boolean =>
  manufacturerRoles.some(manufacturerRole => manufacturerRole === role);

export const isManufacturerAdmin = (role: $Keys<typeof accountRoles>): boolean =>
  role === accountRoles.ROLE_MANUFACTURER_ADMIN;

export const isManufacturerTransporter = (role: $Keys<typeof accountRoles>): boolean =>
  role === accountRoles.ROLE_MANUFACTURER_TRANSPORTER;

export const isUserRole = (role: $Keys<typeof accountRoles>): boolean => role === accountRoles.ROLE_USER;
