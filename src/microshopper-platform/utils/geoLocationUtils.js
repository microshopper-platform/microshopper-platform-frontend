// @flow

const positionOptions: PositionOptions = {
  enableHighAccuracy: false,
  timeout: 15000,
  maximumAge: 0,
};

const checkSupportForGeolocation = (): boolean => 'geolocation' in navigator;
const checkSupportForWatchPosition = (): boolean =>
  checkSupportForGeolocation() && 'watchPosition' in navigator.geolocation;

export const getCurrentPosition = (setCurrentPositionCallback: Function): number => {
  if (checkSupportForGeolocation()) {
    const { geolocation }: { geolocation: Geolocation } = navigator;
    return geolocation.getCurrentPosition(setCurrentPositionCallback, positionErrorCallback, positionOptions);
  }
  return -1;
};

export const watchPosition = (setCurrentPositionCallback: Function): number => {
  if (checkSupportForWatchPosition()) {
    const { geolocation }: { geolocation: Geolocation } = navigator;
    return geolocation.watchPosition(setCurrentPositionCallback, positionErrorCallback, positionOptions);
  }
  return -1;
};

export const clearWatch = (id: number) => {
  const { geolocation }: { geolocation: Geolocation } = navigator;
  geolocation.clearWatch(id);
};

const positionErrorCallback = (error: PositionError) => {
  switch (error.code) {
    case error.PERMISSION_DENIED:
      console.error('User denied the request for Geolocation.');
      break;
    case error.POSITION_UNAVAILABLE:
      console.error('Location information is unavailable.');
      break;
    case error.TIMEOUT:
      console.error('The request to get user location timed out.');
      break;
    default:
      console.error('An unknown error occurred.');
  }
};
