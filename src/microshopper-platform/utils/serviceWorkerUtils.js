// @flow
// eslint-disable-next-line no-unused-vars
let referenceToPrompt = null;

export const listenForBeforeInstallPrompt = () => {
  window.addEventListener('beforeinstallprompt', (e: Object) => {
    referenceToPrompt = e;
  });
};
