// @flow

const isValueBlank = value => value !== 0 && !value;

export const required = (message: string) => (value: string | number) => (!isValueBlank(value) ? undefined : message);

export const minValue = (message: string, min: number) => (value: number) =>
  !isValueBlank(value) && value >= min ? undefined : message;

export const maxValue = (message: string, max: number) => (value: number) =>
  !isValueBlank(value) && value <= max ? undefined : message;

export const composeValidators = (...validators: Array<Function>) => (value: string | number, allValues: Object) =>
  validators.reduce((error, validator: Function) => error || validator(value, allValues), undefined);
