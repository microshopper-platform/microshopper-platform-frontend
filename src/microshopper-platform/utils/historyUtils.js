// @flow
import { browserHistory } from 'microshopper-platform/redux/store';
import type { LocationShape } from 'react-router-dom';

export const redirectTo = (path: string) => {
  browserHistory.push(path);
};

export const redirectToWithState = (locationShape: LocationShape) => {
  browserHistory.push(locationShape);
};
