// @flow

export type RatingBadgeProperties = {
  text: string,
  color: string,
};
