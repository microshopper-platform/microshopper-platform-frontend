// @flow

let wakeLock: Object = null;

const checkSupportForWakeLock = (): boolean => 'wakeLock' in navigator && 'request' in navigator.wakeLock;

export const requestWakeLock = async (type: string) => {
  if (checkSupportForWakeLock()) {
    try {
      wakeLock = await navigator.wakeLock.request(type);
    } catch (err) {
      console.error(`${err.name}, ${err.message}`);
    }
  }
};

export const releaseWakeLock = () => {
  wakeLock.release();
  wakeLock = null;
};

const handleVisibilityChangeCallback = () => {
  if (wakeLock !== null && document.visibilityState === 'visible') {
    requestWakeLock();
  }
};

export const enableVisibilityChange = () => {
  document.addEventListener('visibilitychange', handleVisibilityChangeCallback);
  document.addEventListener('fullscreenchange', handleVisibilityChangeCallback);
};

export const disableVisibilityChange = () => {
  document.removeEventListener('visibilitychange', handleVisibilityChangeCallback);
  document.removeEventListener('fullscreenchange', handleVisibilityChangeCallback);
};
