// @flow

import type { RatingBadgeProperties } from 'microshopper-platform/utils/types.flow';

export const pushNotificationsPublicApiKey: string =
  'BNfyBqnzZRg0qlzf13NgXoO5aJN7nRbb_1ogk_tmyDRff5iCHKD3bePsZ8CDJiG_2db3ItK9Xgr2ccIdxQmnkGc';

export const mapRatingsToBadgeProperties = new Map<string, RatingBadgeProperties>([
  ['EXCELLENT', { text: 'EXCELLENT', color: 'green' }],
  ['VERY_GOOD', { text: 'VERY GOOD', color: 'blue' }],
  ['GOOD', { text: 'GOOD', color: 'yellow' }],
  ['BAD', { text: 'BAD', color: 'orange' }],
  ['VERY_BAD', { text: 'VERY BAD', color: 'red' }],
]);

export const accountRoles = {
  ROLE_USER: 'ROLE_USER',
  ROLE_MANUFACTURER_ADMIN: 'ROLE_MANUFACTURER_ADMIN',
  ROLE_MANUFACTURER_TRANSPORTER: 'ROLE_MANUFACTURER_TRANSPORTER',
};

export const manufacturerRoles: Array<$Keys<typeof accountRoles>> = [
  accountRoles.ROLE_MANUFACTURER_ADMIN,
  accountRoles.ROLE_MANUFACTURER_TRANSPORTER,
];

export const orderStatus = {
  REQUESTED: 'REQUESTED',
  IN_PROGRESS: 'IN_PROGRESS',
  TRACEABLE: 'TRACEABLE',
  COMPLETED: 'COMPLETED',
  CANCELED: 'CANCELED',
};
