// @flow
/* eslint no-restricted-globals: "off" */
import * as precaching from 'workbox-precaching';
import * as routing from 'workbox-routing';
import * as strategies from 'workbox-strategies';
import * as expiration from 'workbox-expiration';
import * as cacheableResponse from 'workbox-cacheable-response';
import notificationLogo from 'microshopper-platform/assets/images/logo/favicon.png';
import type { ApplicationNotification } from 'microshopper-platform/redux/notification/types.flow';

precaching.precacheAndRoute(self.__precacheManifest || []);

// eslint-disable-next-line no-unused-vars
self.addEventListener('install', (event: InstallEvent) => {
  self.skipWaiting();
});

self.addEventListener('message', (event: MessageEvent) => {
  console.log('Message Event', event);
  if (event.data && event.data.type === 'SKIP_WAITING') {
    console.log('Skip Waiting Message Event', event);
    self.skipWaiting();
  }
});

self.addEventListener('activate', (event: Object) => {
  event.waitUntil(self.clients.claim());
});

routing.registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new strategies.StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets',
  }),
);

routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  new strategies.CacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  }),
);

routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|webp|svg)$/,
  new strategies.CacheFirst({
    cacheName: 'images',
    plugins: [
      new expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
    ],
  }),
);

routing.registerRoute(
  /\.(?:woff2)$/,
  new strategies.CacheFirst({
    cacheName: 'fonts',
    plugins: [
      new expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
    ],
  }),
);

routing.registerRoute(
  /\.(?:js|css)$/,
  new strategies.StaleWhileRevalidate({
    cacheName: 'static-resources',
  }),
);

routing.registerRoute(
  new RegExp('^(http|https)://.*:9999\\/api\\/(?!notifications$|oauth$|auth$|orders/track.*$).*'),
  new strategies.NetworkFirst({
    cacheName: 'api-resources',
    plugins: [
      new cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  }),
);

self.addEventListener('push', (event: Object) => {
  const notification: ApplicationNotification = event.data.json();

  const options = {
    body: notification.message,
    icon: notificationLogo,
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: notification.id,
    },
  };
  self.clients.matchAll().then((clients: Array<Client>) => {
    if (clients.length === 0) {
      event.waitUntil(self.registration.showNotification(notification.title, options));
    } else {
      // Send a message to the page to update the UI
      console.log('Application is already open!');
    }
  });
});
