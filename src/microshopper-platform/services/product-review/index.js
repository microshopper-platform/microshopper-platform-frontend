// @flow
import { Configuration, ProductReviewApi } from 'microshopper-platform/api/generated-sources/product-review/src';
import type { ProductReview } from 'microshopper-platform/api/generated-sources/product-review/src';
import { getBearerAuthorizationHeader } from 'microshopper-platform/services/auth/utils';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const api = new ProductReviewApi(configuration);

export const getAllReviews = (): Promise<Array<ProductReview>> => api.getAllReviews();

export const getAllProductReviews = (id: number): Promise<Array<ProductReview>> => api.getAllProductReviews(id);

export const addProductReview = (productReview: ProductReview) =>
  api.addProductReview(productReview, getBearerAuthorizationHeader());
