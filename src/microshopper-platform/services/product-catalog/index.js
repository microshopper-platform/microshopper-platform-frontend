// @flow

import { Configuration, ProductCatalogApi } from 'microshopper-platform/api/generated-sources/product-catalog/src';
import type {
  CompleteProductInformation,
  Product,
  Manufacturer,
} from 'microshopper-platform/api/generated-sources/product-catalog/src';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const api = new ProductCatalogApi(configuration);

export const getCompleteProductInformation = (id: number): Promise<CompleteProductInformation> => api.getProduct(id);

export const getTopSoldProducts = (top: number): Promise<Array<Product>> => api.getTopSoldProducts(top);

export const getTopReviewedProducts = (top: number): Promise<Array<Product>> => api.getTopReviewedProducts(top);

export const getTopManufacturers = (top: number): Promise<Array<Manufacturer>> => api.getTopManufacturers(top);
