// @flow
import type {
  Manufacturer,
  ManufacturerAccount,
  ManufacturerRegistration,
  ManufacturerAccountRegistration,
  GenerateRegistrationCode,
  RegistrationCode,
} from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { ManufacturerApi, Configuration } from 'microshopper-platform/api/generated-sources/manufacturer/src';
import { getBearerAuthorizationHeader } from 'microshopper-platform/services/auth/utils';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const api = new ManufacturerApi(configuration);

export const getAllManufacturers = (): Promise<Array<Manufacturer>> => api.getAllManufacturers();

export const getManufacturerAccountByUsername = (username: string): Promise<ManufacturerAccount> =>
  api.getManufacturerAccountByUsername(username, getBearerAuthorizationHeader());

export const updateManufacturerInformation = (manufacturerInformation: Manufacturer) =>
  api.updateManufacturerInformation(manufacturerInformation, getBearerAuthorizationHeader());

export const registerManufacturer = (manufacturerRegistration: ManufacturerRegistration) =>
  api.registerManufacturer(manufacturerRegistration);

export const registerManufacturerAccount = (manufacturerAccountRegistration: ManufacturerAccountRegistration) =>
  api.registerManufacturerAccount(manufacturerAccountRegistration);

export const generateNewRegistrationCode = (
  generateRegistrationCode: GenerateRegistrationCode,
): Promise<RegistrationCode> =>
  api.generateNewRegistrationCode(generateRegistrationCode, getBearerAuthorizationHeader());

export const getAllRegistrationCodes = (manufacturerId: number): Promise<Array<RegistrationCode>> =>
  api.getAllRegistrationCodes(manufacturerId, getBearerAuthorizationHeader());

export const getAllManufacturerTransporters = (id: number): Promise<Array<ManufacturerAccount>> =>
  api.getAllManufacturerTransporters(id, getBearerAuthorizationHeader());

export const updateManufacturerAccount = (manufacturerAccount: ManufacturerAccount) =>
  api.updateManufacturerAccount(manufacturerAccount, getBearerAuthorizationHeader());
