// @flow
import isEmpty from 'lodash/isEmpty';

const HTTP = 'http';
const HTTPS = 'https';
const DEFAULT_IP_ADDRESS = 'localhost';
const DEFAULT_PORT = '9999';

export const resolveGatewayLocation = (): string => {
  let location;
  if (!isEmpty(process.env.REACT_APP_USE_HTTPS) && Boolean(process.env.REACT_APP_USE_HTTPS)) {
    location = HTTPS;
  } else {
    location = HTTP;
  }

  if (!isEmpty(process.env.REACT_APP_MACHINE_IP_ADDRESS)) {
    location = `${location}://${process.env.REACT_APP_MACHINE_IP_ADDRESS}:${DEFAULT_PORT}`;
  } else {
    location = `${location}://${DEFAULT_IP_ADDRESS}:${DEFAULT_PORT}`;
  }
  return location;
};
