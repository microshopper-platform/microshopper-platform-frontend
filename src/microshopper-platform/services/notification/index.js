// @flow
import { Configuration, NotificationApi } from 'microshopper-platform/api/generated-sources/notification/src';
import { getBearerAuthorizationHeader } from 'microshopper-platform/services/auth/utils';
import type { Subscription } from 'microshopper-platform/api/generated-sources/notification/src';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const notificationApi = new NotificationApi(configuration);

export const deleteNotification = (id: string) =>
  notificationApi.deleteNotification(id, getBearerAuthorizationHeader());

export const markNotificationAsSeen = (id: string) =>
  notificationApi.markNotificationAsSeen(id, getBearerAuthorizationHeader());

export const subscribeForPushNotifications = (username: string, subscription: Subscription) =>
  notificationApi.subscribeForPushNotifications(username, subscription, getBearerAuthorizationHeader());
