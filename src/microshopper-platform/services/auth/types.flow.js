// @flow

import { accountRoles } from 'microshopper-platform/utils/constants';

export type TokenData = {
  access_token: string,
  token_type: string,
  refresh_token: string,
  expires_in: number,
  score: string,
  role: $Keys<typeof accountRoles>,
  username: string,
  id: number,
  externalId: number,
  email: string,
};

export type LoginProps = {
  username: string,
  password: string,
};
