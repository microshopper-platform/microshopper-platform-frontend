// @flow
import { stringify } from 'qs';
import axios from 'axios';
import type { LoginProps, TokenData } from 'microshopper-platform/services/auth/types.flow';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const authEndpoint = `${path}/oauth/token`;

const clientId = 'microshopper-platform';
const clientSecret = 'microshopper-platform-secret';

const contentType = 'application/x-www-form-urlencoded';

const data = {
  grant_type: 'password',
};

export const login = (loginData: LoginProps): Promise<TokenData> =>
  axios({
    url: authEndpoint,
    method: 'post',
    auth: {
      username: clientId,
      password: clientSecret,
    },
    headers: {
      'Content-Type': contentType,
    },
    data: stringify({
      ...data,
      username: loginData.username,
      password: loginData.password,
    }),
  }).then(response => response.data);
