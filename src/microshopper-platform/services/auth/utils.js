// @flow
import Cookies from 'js-cookie';
import isEmpty from 'lodash/isEmpty';
import type { TokenData } from 'microshopper-platform/services/auth/types.flow';

const tokenCookieKey = 'microshopper-platform';

export const saveTokenToCookies = (tokenData: TokenData) => {
  Cookies.set(tokenCookieKey, JSON.stringify(tokenData), { expires: tokenData.expires_in / 60 / 60 / 24 });
};

export const getTokenFromCookies = (): ?TokenData => {
  const cookie = Cookies.get(tokenCookieKey);
  if (!isEmpty(cookie)) {
    return JSON.parse(cookie);
  }
  return null;
};

export const deleteTokenFromCookies = () => Cookies.remove(tokenCookieKey);

export const getBearerAuthorizationHeader = (): Object => {
  const tokenData = getTokenFromCookies();
  if (tokenData) {
    return {
      headers: {
        Authorization: `Bearer ${tokenData.access_token}`,
      },
    };
  }
  return {};
};
