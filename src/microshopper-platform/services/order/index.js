// @flow
import { Configuration, OrderApi } from 'microshopper-platform/api/generated-sources/order/src';
import { getBearerAuthorizationHeader } from 'microshopper-platform/services/auth/utils';
import type {
  Order,
  NewOrder,
  OrderStatusEnum,
  UpdateOrderLocation,
} from 'microshopper-platform/api/generated-sources/order/src';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const orderApi = new OrderApi(configuration);

export const getOrderById = (id: string): Promise<Order> => orderApi.getOrderById(id, getBearerAuthorizationHeader());

export const getAllOrdersForUsername = (username: string): Promise<Array<Order>> =>
  orderApi.getAllOrdersForUsername(username, getBearerAuthorizationHeader());

export const getAllOrdersForManufacturerId = (manufacturerId: number): Promise<Array<Order>> =>
  orderApi.getAllOrdersForManufacturerId(manufacturerId, getBearerAuthorizationHeader());

export const addNewOrder = (newOrder: NewOrder) => orderApi.addNewOrder(newOrder, getBearerAuthorizationHeader());

export const cancelOrder = (id: string) => orderApi.cancelOrder(id, getBearerAuthorizationHeader());

export const updateOrderStatus = (id: string, newStatus: OrderStatusEnum) =>
  orderApi.updateOrderStatus(id, newStatus, getBearerAuthorizationHeader());

export const updateOrderLocation = (id: string, location: UpdateOrderLocation) =>
  orderApi.updateOrderLocation(id, location, getBearerAuthorizationHeader());

export const assignOrderTransporter = (id: string, transporterAssignee: string) =>
  orderApi.assignOrderTransporter(id, transporterAssignee, getBearerAuthorizationHeader());

export const getAllOrdersForTransporterAssingee = (username: string): Promise<Array<Order>> =>
  orderApi.getAllOrdersForTransporterAssignee(username, getBearerAuthorizationHeader());
