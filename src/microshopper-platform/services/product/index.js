// @flow

import { Configuration, ProductApi, CategoryApi } from 'microshopper-platform/api/generated-sources/product/src';
import type { Product, Category, AddProduct } from 'microshopper-platform/api/generated-sources/product/src';
import { getBearerAuthorizationHeader } from 'microshopper-platform/services/auth/utils';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';
import type { SearchProps } from 'microshopper-platform/pages/product-catalog/types.flow';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const productApi = new ProductApi(configuration);
const categoryApi = new CategoryApi(configuration);

export const getAllProducts = (): Promise<Array<Product>> => productApi.getAllProducts();

export const getAllCategories = (): Promise<Array<Category>> => categoryApi.getAllCategories();

export const getProduct = (id: number): Promise<Product> => productApi.getProduct(id);

export const getAllManufacturerProducts = (manufacturerId: number): Promise<Array<Product>> =>
  productApi.getAllManufacturerProducts(manufacturerId);

export const addProduct = (product: AddProduct) => productApi.addProduct(product, getBearerAuthorizationHeader());

export const editProduct = (id: number, product: Product) =>
  productApi.updateProduct(id, product, getBearerAuthorizationHeader());

export const deleteProduct = (id: number) => productApi.deleteProduct(id, getBearerAuthorizationHeader());

export const searchProducts = (searchParameters: SearchProps): Promise<Array<Product>> =>
  productApi.searchProducts(searchParameters.name, searchParameters.manufacturerId, searchParameters.categoryId);
