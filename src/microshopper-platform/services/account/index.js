// @flow
import { AccountApi, Configuration } from 'microshopper-platform/api/generated-sources/account/src';
import type {
  Account,
  AccountData,
  AccountRegistration,
} from 'microshopper-platform/api/generated-sources/account/src';
import { getBearerAuthorizationHeader } from 'microshopper-platform/services/auth/utils';
import { resolveGatewayLocation } from 'microshopper-platform/services/utils';

const path = resolveGatewayLocation();

const configuration = new Configuration({ basePath: path });

const api = new AccountApi(configuration);

export const getAccountByUsername = (username: string): Promise<Account> =>
  api.getAccountByUsername(username, getBearerAuthorizationHeader());

export const getAccountDataByUsername = (username: string): Promise<AccountData> =>
  api.getAccountDataByUsername(username, getBearerAuthorizationHeader());

export const updateAccount = (account: Account) => api.updateAccount(account, getBearerAuthorizationHeader());

export const registerAccount = (accountRegistration: AccountRegistration) => api.registerAccount(accountRegistration);
