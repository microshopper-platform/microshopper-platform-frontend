const WebpackBeforeBuildPlugin = require('before-build-webpack');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');
const path = require('path');
const merge = require('lodash/merge');
const fs = require('fs');

// from https://www.viget.com/articles/run-multiple-webpack-configs-sequentially/
class WaitPlugin extends WebpackBeforeBuildPlugin {
  constructor(file, interval = 100, timeout = 60e3) {
    super(function(stats, callback) {
      const start = Date.now();

      function poll() {
        if (fs.existsSync(file)) {
          callback()
        } else if (Date.now() - start > timeout) {
          throw Error(`Couldn't access ${file} within ${timeout}s`)
        } else {
          setTimeout(poll, interval)
        }
      }
      poll()
    })
  }
}

const swOutputName = 'serviceWorkerCustom.js';
const workerSource = path.resolve(__dirname, 'src/microshopper-platform/service-worker', swOutputName);

module.exports = {
  webpack: (config, env) => {
    // we need 2 webpack configurations:
    // 1- for the service worker file.
    //    it needs to be processed by webpack (to include 3rd party modules), and the output must be a
    //    plain, single file, not injected in the HTML page
    const swConfig = merge({}, config, {
      name: 'serviceWorker',
      entry: workerSource,
      output: {
        filename: swOutputName
      },
      optimization: {
        splitChunks: false,
        runtimeChunk: false
      }
    });
    delete swConfig.plugins;

    // 2- for the main application.
    //    we'll reuse configuration from create-react-app, without a specific Workbox configuration,
    //    so it could inject workbox-precache module and the computed manifest into the BUILT service-worker.js file.
    //    this require to WAIT for the first configuration to be finished
    if (env === 'production') {
      const builtWorkerPath = path.resolve(config.output.path, swOutputName);
      config.name = 'main-application';
      config.plugins.push(
        new WorkboxWebpackPlugin.InjectManifest({
          swSrc: builtWorkerPath,
          swDest: swOutputName,
          globDirectory: 'build',
          globPatterns: ['**/*.{js,css,html,png,svg}'],
          maximumFileSizeToCacheInBytes: 5 * 1024 * 1024,
          importWorkboxFrom: 'disabled',
        }),
        new WaitPlugin(builtWorkerPath),
      )
    }

    // remove Workbox service-worker.js generator
    const removed = config.plugins.findIndex(
      ({ constructor: { name } }) => name === 'GenerateSW'
    );
    if (removed !== -1) {
      config.plugins.splice(removed, 1)
    }

    const result = [swConfig, config];
    // compatibility hack for CRA's build script to support multiple configurations
    // https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/scripts/build.js#L119
    result.output = { publicPath: config.output.publicPath };
    return result;
  },
  devServer: function(configFunction) {
    // Return the replacement function for create-react-app to use to generate the Webpack
    // Development Server config. "configFunction" is the function that would normally have
    // been used to generate the Webpack Development server config - you can use it to create
    // a starting configuration to then modify instead of having to create a config from scratch.
    return function(proxy, allowedHost) {
      // Create the default config by calling configFunction with the proxy/allowedHost parameters
      const config = configFunction(proxy, allowedHost);

      // Change the https certificate options to match your certificate, using the .env file to
      // set the file paths & passphrase.
      config.https = {
        pfx: fs.readFileSync(process.env.REACT_APP_CERTIFICATE_LOCATION),
        passphrase: process.env.REACT_APP_CERTIFICATE_PASSPHRASE,
      };

      // Return your customised Webpack Development Server config.
      return config;
    };
  },
};
